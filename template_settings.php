<div id="settings-holder">
    <script>mw.lib.require('bootstrap3ns');</script>
    <div class="bootstrap3ns">
        <?php

        $shopping_cart = get_option('shopping-cart', 'mw-template-smarty');
        if ($shopping_cart == '') {
            $shopping_cart = 'false';
        } else {
            $shopping_cart = 'true';
        }

        $search_field = get_option('search-field', 'mw-template-smarty');
        if ($search_field == '') {
            $search_field = 'false';
        } else {
            $search_field = 'true';
        }

        $top_bar = get_option('top-bar', 'mw-template-smarty');
        if ($top_bar == '') {
            $top_bar = 'false';
        } else {
            $top_bar = 'true';
        }

        $color_scheme = get_option('color-scheme', 'mw-template-smarty');
        if ($color_scheme == '') {
            $color_scheme = 'green';
        }

        $layout_style = get_option('layout-style', 'mw-template-smarty');
        if ($layout_style == '') {
            $layout_style = 'light';
        }

        $layout_type = get_option('layout-type', 'mw-template-smarty');
        if ($layout_type == '') {
            $layout_type = 'wide';
        }

        $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
        if ($header_breadcrumb_small == '') {
            $header_breadcrumb_small = 'false';
        } else {
            $header_breadcrumb_small = 'true';
        }

        $preloader = get_option('preloader', 'mw-template-smarty');
        if ($preloader == '') {
            $preloader = 'false';
        } else {
            $preloader = 'true';
        }

        $header_style = get_option('header-style', 'mw-template-smarty');
        if ($header_style == '') {
            $header_style = 'clean';
        }

        $rtl_is_on = get_option('rtl_is_on', 'mw-template-smarty');
        if ($rtl_is_on == '') {
            $rtl_is_on = 'false';
        } else {
            $rtl_is_on = 'true';
        }

        $footer_style = get_option('footer-style', 'mw-template-smarty');
        if ($footer_style == '') {
            $footer_style = 'default';
        }
        ?>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="mw_option_field" id="shopping-cart" name="shopping-cart" data-option-group="mw-template-smarty" value="true" <?php if ($shopping_cart == 'true') {
                            echo 'checked';
                        } ?> /> Show shopping cart in header
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="mw_option_field" id="search-field" name="search-field" data-option-group="mw-template-smarty" value="true" <?php if ($search_field == 'true') {
                            echo 'checked';
                        } ?> /> Show search field in header
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="mw_option_field" id="top-bar" name="top-bar" data-option-group="mw-template-smarty" value="true" <?php if ($top_bar == 'true') {
                            echo 'checked';
                        } ?> /> Show Top Bar above Header
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="mw_option_field" id="preloader" name="preloader" data-option-group="mw-template-smarty" value="true" <?php if ($preloader == 'true') {
                            echo 'checked';
                        } ?> /> Turn on Page Preloader
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="select" class="col-lg-2 control-label">Header Style</label>
            <div class="col-lg-10">
                <select name="header-style" id="header-style" class="mw_option_field form-control" data-option-group="mw-template-smarty">
                    <option value="clean" <?php if ($header_style == '' OR $header_style == 'clean') {
                        echo 'selected';
                    } ?>>Clean
                    </option>
                    <option value="background"<?php if ($header_style == 'background') {
                        echo 'selected';
                    } ?>>Background
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group <?php if ($header_style == 'background') { echo 'hidden'; } ?>">
            <div class="col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="mw_option_field" id="header-breadcrumb-small" name="header-breadcrumb-small" data-option-group="mw-template-smarty" value="true" <?php if ($header_breadcrumb_small == 'true') {
                            echo 'checked';
                        } ?> /> Make breadcrumbs header small ?
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="select" class="col-lg-2 control-label">Color scheme</label>
            <div class="col-lg-10">
                <select name="color-scheme" id="color-scheme" class="mw_option_field form-control" data-option-group="mw-template-smarty">
                    <option value="green" <?php if ($color_scheme == '' OR $color_scheme == 'green') {
                        echo 'selected';
                    } ?>>Green
                    </option>
                    <option value="red"<?php if ($color_scheme == 'red') {
                        echo 'selected';
                    } ?>>Red
                    </option>
                    <option value="orange"<?php if ($color_scheme == 'orange') {
                        echo 'selected';
                    } ?>>Orange
                    </option>
                    <option value="pink"<?php if ($color_scheme == 'pink') {
                        echo 'selected';
                    } ?>>Pink
                    </option>
                    <option value="yellow"<?php if ($color_scheme == 'yellow') {
                        echo 'selected';
                    } ?>>Yellow
                    </option>
                    <option value="darkgreen"<?php if ($color_scheme == 'darkgreen') {
                        echo 'selected';
                    } ?>>Dark Green
                    </option>
                    <option value="darkblue"<?php if ($color_scheme == 'darkblue') {
                        echo 'selected';
                    } ?>>Dark Blue
                    </option>
                    <option value="blue"<?php if ($color_scheme == 'blue') {
                        echo 'selected';
                    } ?>>Blue
                    </option>
                    <option value="brown"<?php if ($color_scheme == 'brown') {
                        echo 'selected';
                    } ?>>Brown
                    </option>
                    <option value="lightgrey"<?php if ($color_scheme == 'lightgrey') {
                        echo 'selected';
                    } ?>>Light Grey
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="select" class="col-lg-2 control-label">Layout Style</label>
            <div class="col-lg-10">
                <select name="layout-style" id="layout-style" class="mw_option_field form-control" data-option-group="mw-template-smarty">
                    <option value="light" <?php if ($layout_style == '' OR $layout_style == 'light') {
                        echo 'selected';
                    } ?>>Light
                    </option>
                    <option value="dark"<?php if ($layout_style == 'dark') {
                        echo 'selected';
                    } ?>>Dark
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="select" class="col-lg-2 control-label">Layout Type</label>
            <div class="col-lg-10">
                <select name="layout-type" id="layout-type" class="mw_option_field form-control" data-option-group="mw-template-smarty">
                    <option value="wide" <?php if ($layout_type == '' OR $layout_type == 'wide') {
                        echo 'selected';
                    } ?>>Wide
                    </option>
                    <option value="boxed"<?php if ($layout_type == 'boxed') {
                        echo 'selected';
                    } ?>>Boxed
                    </option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="mw_option_field" id="rtl_is_on" name="rtl_is_on" data-option-group="mw-template-smarty" value="true" <?php if ($rtl_is_on == 'true') {
                            echo 'checked';
                        } ?> /> Turn on RTL
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="select" class="col-lg-2 control-label">Footer Style</label>
            <div class="col-lg-10">
                <select name="footer-style" id="layout-type" class="mw_option_field form-control" data-option-group="mw-template-smarty">
                    <option value="default" <?php if ($footer_style == '' OR $footer_style == 'default') {
                        echo 'selected';
                    } ?>>Default
                    </option>
                    <option value="style-1"<?php if ($footer_style == 'style-1') {
                        echo 'selected';
                    } ?>>Style 1
                    </option>
                </select>
            </div>
        </div>
    </div>
</div>
<!-- /#settings-holder -->