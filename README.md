# Smarty

Most importantly, with the extensive features of Smarty Template, you can be sure that your online presence reflects the energy and diversity of your Business.

Smarty is a powerful and easy to use Microweber Template. It allows you to create any kind of web templates like complete websites, landing pages, coming soon, homepages, etc in no-time. It comes with some stunning pre-build page templates to make your life even easier to use it directly for your projects.

## Features:

* 20 Page Templates (Home, Services, Portfolio, Blog ... others)
* eCommerce Support  (Shop Pag, Products Feed, Shopping Cart and Checkout Page)
* Pixel Perfect Design
* Fully Responsive Layout
* User-Friendly Code
* Clean Markup
* Creative Design
* Multiple Skins Sliders
* Fully Responsive
* Cross Browser Support
* Easy to Customize
* Video embedding
* Multiple Fonts Supported
* Well Documented
* Free Lifetime Updates
* Community Support