<?php $footer_style = get_option('footer-style', 'mw-template-smarty'); ?>
<?php if ($footer_style == 'style-1'): ?>
    <footer id="footer" class="nodrop">
        <div class="container edit" field="smarty_footer_1" rel="global">

            <div class="row">
                <div class="col-md-8 allow-drop">
                    <h3 class="letter-spacing-1">WHY US?</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet hendrerit volutpat. Sed in nunc nec ligula consectetur mollis in vel justo. Vestibulum ante ipsum.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet hendrerit volutpat.
                    </p>
                    <h2>(800) 123-4567</h2>
                </div>

                <div class="col-md-4">
                    <h3 class="letter-spacing-1">KEEP IN TOUCH</h3>

                    <p>Subscribe to Our Newsletter to get Important News & Offers</p>

                    <module type="newsletter" template="skin-1"/>
                </div>
            </div>


        </div>

        <div class="copyright">
            <div class="container">
                <module type="menu" template="footer-bottom" name="footer_bottom_menu" id="footer_bottom_menu" class=""/>
                <?php print powered_by_link(); ?> | <a href="<?php print admin_url() ?>">Admin</a>
            </div>
        </div>
    </footer>
<?php else: ?>
    <footer id="footer" class="nodrop">
        <div class="container edit" field="smarty_footer" rel="global">

            <div class="row margin-top-60 margin-bottom-40 size-13">
                <div class="col-md-4 col-sm-4">
                    <module type="logo" id="footer-logo" template="footer"/>
                    <div class="allow-drop">
                        <p>Incredibly beautiful responsive Bootstrap Template for Corporate and Creative Professionals.</p>

                        <h2>(800) 123-4567</h2>
                    </div>

                    <module type="social_links" id="socials"/>
                </div>

                <div class="col-md-8 col-sm-8">
                    <div class="row">
                        <div class="col-md-5 hidden-sm hidden-xs allow-drop">
                            <h4 class="letter-spacing-1">RECENT NEWS</h4>

                            <module type="posts" template="skin-6" limit="3"/>
                        </div>

                        <div class="col-md-3 hidden-sm hidden-xs allow-drop">
                            <h4 class="letter-spacing-1">EXPLORE US</h4>
                            <module type="menu" name="footer_menu" id="footer-navigation" template="footer"/>
                        </div>

                        <div class="col-md-4 allow-drop">
                            <h4 class="letter-spacing-1">SECURE PAYMENT</h4>
                            <p>Donec tellus massa, tristique sit amet condim vel, facilisis quis sapien. Praesent id enim sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="copyright">
            <div class="container">
                <module type="menu" template="footer-bottom" name="footer_bottom_menu" id="footer_bottom_menu" class="pull-right nomargin list-inline mobile-block"/>
                <?php print powered_by_link(); ?> | <a href="<?php print admin_url() ?>">Admin</a>
            </div>
        </div>
    </footer>

<?php endif; ?>

</div>

<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>


<?php $preloader = get_option('preloader', 'mw-template-smarty'); ?>
<?php if ($preloader != '' AND $preloader == 'true'): ?>
    <div id="preloader">
        <div class="inner">
            <span class="loader"></span>
        </div>
    </div>
<?php endif; ?>

<!-- JAVASCRIPT FILES -->
<script type="text/javascript">var plugin_path = '{TEMPLATE_URL}assets/plugins/';</script>
<script type="text/javascript" src="{TEMPLATE_URL}assets/js/scripts.js"></script>
<!-- SWIPE SLIDER -->
<script type="text/javascript" src="{TEMPLATE_URL}assets/plugins/slider.swiper/dist/js/swiper.min.js"></script>
<!-- OWL SLIDER -->
<script type="text/javascript" src="{TEMPLATE_URL}assets/plugins/owl-carousel/owl.carousel.js"></script>
<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="{TEMPLATE_URL}assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="{TEMPLATE_URL}assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
<!--<script type="text/javascript" src="{TEMPLATE_URL}assets/js/view/demo.revolution_slider.js"></script>-->

</body>
</html>