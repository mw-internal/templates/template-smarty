<?php

/*

  type: layout
  content_type: static
  name: Home
  position: 11
  description: Home layout

*/

?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <module type="layouts" template="skin-9"/>
    <module type="layouts" template="skin-10"/>
    <module type="layouts" template="skin-11"/>
</div>

<?php include template_dir() . "footer.php"; ?>
