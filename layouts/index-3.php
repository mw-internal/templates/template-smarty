<?php

/*

  type: layout
  content_type: static
  name: Home 3
  position: 11
  description: Home layout

*/

?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <module type="layouts" template="skin-36"/>
    <module type="layouts" template="skin-12"/>
    <module type="layouts" template="skin-13"/>
</div>

<?php include template_dir() . "footer.php"; ?>
