<?php

/*

type: layout
content_type: dynamic
name: Blog
position: 3
description: Blog

*/


?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <div class="nodrop safe-mode">
        <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
        <?php if ($header_style == '' OR $header_style == 'clean'): ?>
            <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
            if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                <div class="container">
                    <h1 class="edit" field="title" rel="content">Blog</h1>

                    <module type="breadcrumb" id="blog-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php elseif ($header_style == 'background'): ?>
            <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                <div class="container">
                    <h1 class="edit" field="title" rel="content">Blog</h1>
                    <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                    <module type="breadcrumb" id="blog-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php endif; ?>
    </div>
</div>

<section class="nodrop">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <module data-type="posts" template="skin-1" id="blog-posts-<?php print PAGE_ID; ?>"/>
            </div>

            <div class="col-md-3 col-sm-3">
                <?php include_once "blog_sidebar.php"; ?>
            </div>
        </div>
    </div>
</section>

<?php include template_dir() . "footer.php"; ?>






