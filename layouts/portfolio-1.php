<?php

/*

type: layout
content_type: dynamic
name: Portfolio 1
position: 3
description: Portfolio 1

*/


?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <div class="nodrop safe-mode">
        <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
        <?php if ($header_style == '' OR $header_style == 'clean'): ?>
            <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
            if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                <div class="container">
                    <h1 class="edit" field="title" rel="content">Portfolio</h1>

                    <module type="breadcrumb" id="portfolio-1-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php elseif ($header_style == 'background'): ?>
            <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                <div class="container">
                    <h1 class="edit" field="title" rel="content">Portfolio</h1>
                    <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                    <module type="breadcrumb" id="portfolio-1-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php endif; ?>
    </div>

    <module type="layouts" template="skin-15"/>
    <module type="layouts" template="skin-16"/>
</div>

<?php include template_dir() . "footer.php"; ?>






