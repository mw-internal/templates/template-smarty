<?php

/*

type: layout
content_type: static
name: Contact Us

description: Contact us layout
position: 7
*/


?>
<?php include template_dir() . "header.php"; ?>

    <div id="contacts-<?php print CONTENT_ID; ?>">
        <div class="edit" rel="content" field="smarty_content">
            <div class="nodrop safe-mode">
                <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
                <?php if ($header_style == '' OR $header_style == 'clean'): ?>
                    <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
                    if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                        <div class="container">
                            <h1 class="edit" field="title" rel="content">Contacts</h1>

                            <module type="breadcrumb" id="contacts-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php elseif ($header_style == 'background'): ?>
                    <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                        <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                        <div class="container">
                            <h1 class="edit" field="title" rel="content">Contacts</h1>
                            <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                            <module type="breadcrumb" id="contacts-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php endif; ?>
            </div>

            <section class="nodrop safe-mode">
                <div class="container">
                    <div id="map3" class="margin-bottom-30">
                        <module type="google_maps"/>
                        <div class="clearfix"></div>
                    </div>


                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <h3>Drop us a line or just say <strong><em>Hello!</em></strong></h3>
                            <module type="contact_form"/>
                        </div>

                        <div class="col-md-3 col-sm-3 allow-drop">
                            <h2>Visit Us</h2>
                            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.</p>
                            <hr/>

                            <p>
                                <span class="block"><strong><i class="fa fa-map-marker"></i> Address:</strong> Street Name, City Name, Country</span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Phone:</strong> <a href="tel:1800-555-1234">1800-555-1234</a></span>
                                <span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">mail@yourdomain.com</a></span>
                            </p>

                            <hr/>

                            <h4 class="font300">Business Hours</h4>
                            <p>
                                <span class="block"><strong>Monday - Friday:</strong> 10am to 6pm</span>
                                <span class="block"><strong>Saturday:</strong> 10am to 2pm</span>
                                <span class="block"><strong>Sunday:</strong> Closed</span>
                            </p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

<?php include template_dir() . "footer.php"; ?>