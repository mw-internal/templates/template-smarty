<?php

/*

type: layout
content_type: static
name: FAQ

description: FAQ layout
position: 7
*/


?>
<?php include template_dir() . "header.php"; ?>

    <div id="faq-<?php print CONTENT_ID; ?>">
        <div class="edit" rel="content" field="smarty_content">
            <div class="nodrop safe-mode">
                <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
                <?php if ($header_style == '' OR $header_style == 'clean'): ?>
                    <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
                    if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                        <div class="container">
                            <h1 class="edit" field="title" rel="content">FAQ</h1>

                            <module type="breadcrumb" id="faq-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php elseif ($header_style == 'background'): ?>
                    <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                        <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                        <div class="container">
                            <h1 class="edit" field="title" rel="content">FAQ</h1>
                            <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                            <module type="breadcrumb" id="faq-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php endif; ?>
            </div>

            <section class="nodrop safe-mode">
                <div class="container">
                    <div class="row mix-grid">
                        <div class="col-md-9">
                            <module type="faq"/>

                            <div class="callout alert alert-border margin-top-60 margin-bottom-60">
                                <div class="row">

                                    <div class="col-md-9 col-sm-12"><!-- left text -->
                                        <h4>Call now at <strong>+800-565-2390</strong> and get 15% discount!</strong></h4>
                                        <p class="font-lato size-17">
                                            We truly care about our users and our product.
                                        </p>
                                    </div>

                                    <div class="col-md-3 col-sm-12 text-right"><!-- right btn -->
                                        <a href="#" target="_blank" class="btn btn-default btn-lg">PURCHASE NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="edit allow-drop" rel="smarty_faq_sidebar" field="content">
                                <h4><strong>Poular</strong> Topics</h4>
                                <p><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet.</em></p>

                                <hr/>

                                <ul class="list-unstyled">
                                    <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                                    <li><a href="#">Consectetur adipiscing elit</a></li>
                                    <li><a href="#">Quisque rutrum pellentesque</a></li>
                                </ul>

                                <hr/>

                                <h4><strong>Ask</strong> a question</h4>
                            </div>

                            <module type="contact_form" template="skin-1"/>

                            <hr/>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>

<?php include template_dir() . "footer.php"; ?>