<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <div class="nodrop safe-mode">
        <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
        <?php if ($header_style == '' OR $header_style == 'clean'): ?>
            <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
            if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                <div class="container">
                    <h1 class="edit" field="title" rel="content">BLOG POST TITLE HER</h1>

                    <module type="breadcrumb" id="blog-inner-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php elseif ($header_style == 'background'): ?>
            <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                <div class="container">
                    <h1 class="edit" field="title" rel="content">BLOG POST TITLE HER</h1>
                    <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                    <module type="breadcrumb" id="blog-inner-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php endif; ?>
    </div>

    <section id="blog-content-<?php print CONTENT_ID; ?>">
        <div class="container">

            <div class="row">

                <!-- LEFT -->
                <div class="col-md-9 col-sm-9">

                    <h1 class="blog-post-title"><?php print content_title(); ?></h1>
                    <ul class="blog-post-info list-inline">
                        <li>
                            <a href="#">
                                <i class="fa fa-clock-o"></i>
                                <span class="font-lato"><?php print date('M d, Y', strtotime(content_date())); ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-comment-o"></i>
                                <span class="font-lato"><?php print get_comments('count=1&content_id=' . content_id()) ?> Comments</span>
                            </a>
                        </li>

                        <li>
                            <i class="fa fa-folder-open-o"></i>

                            <?php
                            $categories = content_categories(content_id());
                            if(!empty($categories)):
                            foreach ($categories as $category):
                                ?>
                                <a class="category" href="<?php print category_link($category['id']); ?>">
                                    <span class="font-lato"><?php print $category['title']; ?></span>
                                </a>
                            <?php endforeach; endif; ?>
                        </li>
                        <?php $post = get_content_by_id(CONTENT_ID); ?>

                        <?php if ($post['created_by']): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-user"></i>
                                    <span class="font-lato"><?php print $post['created_by']; ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>

                    <div class="clearfix"></div>
                    <module data-type="pictures" template="skin-1" rel="content"/>
                    <div class="clearfix"></div>

                    <br/>
                    <br/>


                    <div class="edit dropcap" field="content" rel="content">
                        <div class="element">
                            <p align="justify">This text is set by default and is suitable for edit in real time. By default the drag and drop core feature will allow you to position it anywhere on
                                the
                                site. Get creative, Make Web.</p>
                        </div>
                    </div>


                    <div class="divider divider-dotted"><!-- divider --></div>

                    <module type="tags" content_id="<?php print content_id(); ?>"/>

                    <!-- SHARE POST -->
                    <div class="clearfix margin-top-30">
                    <span class="pull-left margin-top-6 bold hidden-xs">
                        <?php _lang('Share Post:', "templates/smarty"); ?>
                    </span>
                        <module type="sharer" id="share-post" class="pull-right"/>
                    </div>
                    <!-- /SHARE POST -->

                    <div class="divider"><!-- divider --></div>


                    <ul class="pager">
                        <?php if (prev_content()): ?>
                            <?php $prev_content = prev_content(CONTENT_ID); ?>
                            <li class="previous"><a class="noborder" href="<?php print $prev_content['url'] ?>">&larr; Previous Post</a></li>
                        <?php endif; ?>
                        <?php if (next_content()): ?>
                            <?php $next_content = next_content(CONTENT_ID); ?>
                            <li class="next"><a class="noborder" href="<?php print $next_content['url'] ?>">Next Post &rarr;</a></li>
                        <?php endif; ?>
                    </ul>

                    <div class="divider"><!-- divider --></div>

                    <div id="comments" class="comments">
                        <h4 class="page-header margin-bottom-60 size-20">
                            <span><?php print get_comments('count=1&content_id=' . content_id()) ?></span> COMMENTS
                        </h4>

                        <div class="edit" rel="content" field="comments">
                            <module type="comments" template="default" data-content-id="<?php print CONTENT_ID; ?>"/>
                        </div>
                    </div>

                </div>

                <div class="col-md-3 col-sm-3">
                    <?php include_once "blog_sidebar.php"; ?>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include template_dir() . "footer.php"; ?>

