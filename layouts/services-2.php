<?php

/*

type: layout
content_type: static
name: Services 2

description: Services 2 layout
position: 7
*/


?>
<?php include template_dir() . "header.php"; ?>

    <div id="services2-<?php print CONTENT_ID; ?>">
        <div class="edit" rel="content" field="smarty_content">
            <div class="nodrop safe-mode">
                <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
                <?php if ($header_style == '' OR $header_style == 'clean'): ?>
                    <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
                    if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                        <div class="container">
                            <h1 class="edit" field="title" rel="content">Services</h1>

                            <module type="breadcrumb" id="services-2-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php elseif ($header_style == 'background'): ?>
                    <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                        <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                        <div class="container">
                            <h1 class="edit" field="title" rel="content">Services</h1>
                            <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                            <module type="breadcrumb" id="services-2-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php endif; ?>
            </div>

            <module type="layouts" template="skin-40"/>

            <section>
                <div class="container">

                    <header class="text-center">
                        <h2 class="nomargin">Features</h2>
                        <p class="font-lato size-20 nomargin">We truly care about our customers</p>
                    </header>

                    <hr/>

                    <div class="row margin-top-80">

                        <div class="col-lg-4 col-md-4 col-md-push-4 text-center">

                            <img class="img-responsive" src="<?php print template_url(); ?>assets/images/iphone-min.png" alt=""/>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-md-pull-4">

                            <div class="box-icon box-icon-right">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-eye"></i>
                                    <h2>Retina Ready</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                            <div class="box-icon box-icon-right">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-check"></i>
                                    <h2>Unlimited Options</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                            <div class="box-icon box-icon-right">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-rocket"></i>
                                    <h2>Premium Sliders</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                            <div class="box-icon box-icon-right">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-flash"></i>
                                    <h2>performance</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6">

                            <div class="box-icon box-icon-left">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-tablet"></i>
                                    <h2>Fully Reposnive</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                            <div class="box-icon box-icon-left">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-random"></i>
                                    <h2>Clean Design</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                            <div class="box-icon box-icon-left">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-tint"></i>
                                    <h2>Reusable Elements</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                            <div class="box-icon box-icon-left">
                                <a class="box-icon-title" href="#">
                                    <i class="fa fa-cogs"></i>
                                    <h2>Multipurpose</h2>
                                </a>
                                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                            </div>

                        </div>

                    </div>


                </div>
            </section>

            <section>
                <div class="container">

                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="size-20">Why chose us?</h3>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check"></i> Fully responsive so your content will always look good on any screen</li>
                                <li><i class="fa fa-check"></i> Awesome sliders give you the opportunity to showcase content</li>
                                <li><i class="fa fa-check"></i> Unlimited color options with a backed color picker, gradients</li>
                                <li><i class="fa fa-check"></i> Multiple layout options for home pages, portfolio &amp; blo</li>
                                <li><i class="fa fa-check"></i> We offer free support because we care about your site.</li>
                            </ul>
                        </div>

                        <div class="col-md-6">
                            <h3 class="size-20 text-center">What Smarty clients say?</h3>
                            <!--
                                Note: remove class="rounded" from the img for squared image!
                            -->
                            <ul class="row clearfix testimonial-dotted list-unstyled">
                                <li class="col-md-6">
                                    <div class="testimonial">
                                        <figure class="text-center">
                                            <img src="<?php print pixum(60, 60); ?>" class="rounded"/>
                                        </figure>
                                        <div class="" style="margin-top: 15px;">
                                            <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam!</p>
                                            <cite>
                                                Joana Doe
                                                <span>Company Ltd.</span>
                                            </cite>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="testimonial">
                                        <figure class="text-center">
                                            <img src="<?php print pixum(60, 60); ?>" class="rounded"/>
                                        </figure>
                                        <div class="" style="margin-top: 15px;">
                                            <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam!</p>
                                            <cite>
                                                Melissa Doe
                                                <span>Company Ltd.</span>
                                            </cite>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            <module type="layouts" template="skin-28"/>
        </div>
    </div>

<?php include template_dir() . "footer.php"; ?>