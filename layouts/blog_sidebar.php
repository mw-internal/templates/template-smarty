<div class="edit" field="smarty_blog_sidebar" rel="inherit">
    <module type="search"/>

    <hr/>

    <!-- side navigation -->
    <div class="side-nav margin-bottom-30 margin-top-30">
        <div class="side-nav-head">
            <button class="fa fa-bars"></button>
            <h4>CATEGORIES</h4>
        </div>

        <module type="categories" content-id="<?php print PAGE_ID; ?>"/>
    </div>

    <div class="nomargin-top hidden-xs margin-bottom-30">
        <module type="tabs"/>
    </div>

    <h3 class="hidden-xs size-16 margin-bottom-20">TAGS</h3>
    <div class="hidden-xs margin-bottom-30">
        <module type="tags"/>
    </div>

    <!-- TWIITER WIDGET -->
    <h3 class="hidden-xs size-16 margin-bottom-10">TWITTER FEED</h3>
    <module type="twitter_feed"/>

    <module type="facebook_page"/>
    <hr/>

    <div class="hidden-xs margin-top-30 margin-bottom-30">
        <module type="social_links"/>
    </div>
</div>
