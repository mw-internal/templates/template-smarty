<?php include template_dir() . "header.php"; ?>

<div id="product-<?php print CONTENT_ID; ?>">
    <div class="edit" rel="content" field="smarty_content">
        <div class="nodrop safe-mode">
            <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
            <?php if ($header_style == '' OR $header_style == 'clean'): ?>
                <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
                if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                    <div class="container">
                        <h1 class="edit" field="title" rel="content">Product name</h1>

                        <module type="breadcrumb" id="product-inner-breadcrumbs-<?php print PAGE_ID; ?>"/>
                    </div>
                </section>
            <?php elseif ($header_style == 'background'): ?>
                <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                    <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                    <div class="container">
                        <h1 class="edit" field="title" rel="content">Product name</h1>
                        <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                        <module type="breadcrumb" id="product-inner-breadcrumbs-<?php print PAGE_ID; ?>"/>
                    </div>
                </section>
            <?php endif; ?>
        </div>

        <section class="nodrop">
            <div class="container">

                <div class="row">

                    <div class="col-lg-4 col-sm-4">
                        <module type="pictures" rel="content" template="skin-2"/>
                    </div>

                    <!-- ITEM DESC -->
                    <div class="col-lg-5 col-sm-8">

                        <?php load_layout_block('shop/shop-info-1'); ?>

                    </div>
                    <!-- /ITEM DESC -->

                    <div class="col-sm-4 col-md-3">
                        <div class="edit allow-drop" field="product_right_side" rel="global">
                            <h4 class="size-18">
                                <i class="fa fa-paper-plane-o safe-element"></i>
                                FREE SHIPPING
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla.</p>

                            <h4 class="size-18">
                                <i class="fa fa-clock-o safe-element"></i>
                                30 DAYS MONEY BACK
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla.</p>

                            <h4 class="size-18">
                                <i class="fa fa-users safe-element"></i>
                                CUSTOMER SUPPORT
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla.</p>

                            <hr>

                            <p class="size-11">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque.
                            </p>
                        </div>
                    </div>

                </div>


                <ul id="myTab" class="nav nav-tabs nav-top-border margin-top-80" role="tablist">
                    <li role="presentation" class="active"><a href="#description" role="tab" data-toggle="tab">Description</a></li>
                    <li role="presentation"><a href="#specs" role="tab" data-toggle="tab">Specifications</a></li>
                    <li role="presentation"><a href="#reviews" role="tab" data-toggle="tab">Reviews</a></li>
                </ul>


                <div class="tab-content padding-top-20">
                    <div role="tabpanel" class="tab-pane fade in active" id="description">
                        <div class="edit allow-drop" field="content_body" rel="post">
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet,
                                ante.
                                Donec
                                eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Aliquam fermentum commodo magna, id pretium nisi elementum at. Nulla molestie, ligula in
                                fringilla
                                rhoncus, risus leo dictum est, nec egestas nunc sem tincidunt turpis. Sed posuere consectetur est at lobortis.</p>
                            <p>Donec blandit ultrices condimentum. Aliquam fermentum commodo magna, id pretium nisi elementum at. Nulla molestie, ligula in fringilla rhoncus, risus leo dictum est, nec
                                egestas
                                nunc sem tincidunt turpis. Sed posuere consectetur est at lobortis.</p>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="specs">
                        <div class="edit" field="product_sheets" rel="post">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Column name</th>
                                        <th>Column name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Size</td>
                                        <td>2XL</td>
                                    </tr>
                                    <tr>
                                        <td>Color</td>
                                        <td>Red</td>
                                    </tr>
                                    <tr>
                                        <td>Weight</td>
                                        <td>132lbs</td>
                                    </tr>
                                    <tr>
                                        <td>Height</td>
                                        <td>74cm</td>
                                    </tr>
                                    <tr>
                                        <td>Bluetooth</td>
                                        <td><i class="fa fa-check text-success"></i> YES</td>
                                    </tr>
                                    <tr>
                                        <td>Wi-Fi</td>
                                        <td><i class="glyphicon glyphicon-remove text-danger"></i> NO</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- REVIEWS -->
                    <div role="tabpanel" class="tab-pane fade" id="reviews">
                        <module type="comments" content-id="<?php print CONTENT_ID; ?>"/>
                    </div>
                </div>

                <hr class="margin-top-80 margin-bottom-80"/>

                <div class="edit" field="related_products" rel="inherit">
                    <h2 class="owl-featured"><strong>Related</strong> products:</h2>
                    <module type="shop/products" template="skin-1" related="true"/>
                </div>

            </div>
        </section>

    </div>
</div>

<?php include template_dir() . "footer.php"; ?>
