<?php

/*

type: layout
content_type: static
name: Services 1

description: Services 1 layout
position: 7
*/


?>
<?php include template_dir() . "header.php"; ?>

    <div id="services1-<?php print CONTENT_ID; ?>">
        <div class="edit" rel="content" field="smarty_content">
            <div class="nodrop safe-mode">
                <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
                <?php if ($header_style == '' OR $header_style == 'clean'): ?>
                    <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
                    if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                        <div class="container">
                            <h1 class="edit" field="title" rel="content">Services</h1>

                            <module type="breadcrumb" id="services-1-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php elseif ($header_style == 'background'): ?>
                    <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                        <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                        <div class="container">
                            <h1 class="edit" field="title" rel="content">Services</h1>
                            <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                            <module type="breadcrumb" id="services-1-breadcrumbs-<?php print PAGE_ID; ?>"/>
                        </div>
                    </section>
                <?php endif; ?>
            </div>

            <module type="layouts" template="skin-29"/>
            <module type="layouts" template="skin-27"/>

            <section>
                <div class="container">
                    <h2 class="size-25">What else Smarty can offer?</h2>

                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin
                        adipiscing
                        porta tellus, ut feugiat nibh adipiscing sit amet.</p>

                    <div class="margin-top-60">
                        <module type="tabs" template="skin-1"/>
                    </div>
                </div>
            </section>

            <section>
                <div class="container">

                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="size-20">Why chose us?</h3>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check"></i> Fully responsive so your content will always look good on any screen</li>
                                <li><i class="fa fa-check"></i> Awesome sliders give you the opportunity to showcase content</li>
                                <li><i class="fa fa-check"></i> Unlimited color options with a backed color picker, gradients</li>
                                <li><i class="fa fa-check"></i> Multiple layout options for home pages, portfolio &amp; blo</li>
                                <li><i class="fa fa-check"></i> We offer free support because we care about your site.</li>
                            </ul>
                        </div>

                        <div class="col-md-6">
                            <h3 class="size-20 text-center">What Smarty clients say?</h3>
                            <!--
                                Note: remove class="rounded" from the img for squared image!
                            -->
                            <ul class="row clearfix testimonial-dotted list-unstyled">
                                <li class="col-md-6">
                                    <div class="testimonial">
                                        <figure class="text-center">
                                            <img src="<?php print pixum(60, 60); ?>" class="rounded"/>
                                        </figure>
                                        <div style="margin-top:15px;">
                                            <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam!</p>
                                            <cite>
                                                Joana Doe
                                                <span>Company Ltd.</span>
                                            </cite>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="testimonial">
                                        <figure class="text-center">
                                            <img src="<?php print pixum(60, 60); ?>" class="rounded"/>
                                        </figure>
                                        <div style="margin-top:15px;">
                                            <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam!</p>
                                            <cite>
                                                Melissa Doe
                                                <span>Company Ltd.</span>
                                            </cite>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            <module type="layouts" template="skin-28"/>
        </div>
    </div>

<?php include template_dir() . "footer.php"; ?>