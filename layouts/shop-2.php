<?php

/*

type: layout
content_type: dynamic
name: Shop 2
is_shop: y
description: shop layout
position: 4
*/


?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <div class="nodrop safe-mode">
        <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
        <?php if ($header_style == '' OR $header_style == 'clean'): ?>
            <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
            if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                <div class="container">
                    <h1 class="edit" field="title" rel="content">SHOP</h1>

                    <module type="breadcrumb" id="shop-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php elseif ($header_style == 'background'): ?>
            <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                <div class="container">
                    <h1 class="edit" field="title" rel="content">SHOP</h1>
                    <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                    <module type="breadcrumb" id="shop-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php endif; ?>
    </div>

    <section class="nodrop">
        <div class="container">

            <div class="row">

                <!-- RIGHT -->
                <div class="col-lg-9 col-md-9 col-sm-9 col-lg-push-3 col-md-push-3 col-sm-push-3">
                    <module type="bxslider" template="owl" id="shop2-slider-<?php print content_id(); ?>"/>

                    <hr/>

                    <module type="shop/products" limit="18" template="skin-4" description-length="70"/>
                </div>

                <!-- LEFT -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
                    <div class="side-nav margin-bottom-60">
                        <div class="side-nav-head">
                            <button class="fa fa-bars"></button>
                            <h4>CATEGORIES</h4>
                        </div>

                        <module type="categories" content_id="<?php print PAGE_ID; ?>" template="default"/>
                    </div>

                    <div class="margin-bottom-60">
                        <a href="#">
                            <img class="img-responsive" src="<?php print thumbnail('', '270'); ?>" width="270" height="270" alt="">
                        </a>
                    </div>

                    <div class="margin-bottom-60">
                        <h2 class="owl-featured">FEATURED</h2>
                        <module type="shop/products" limit="5" template="skin-5" description-length="70"/>
                    </div>

                    <div class="margin-bottom-60">
                        <a href="#">
                            <img class="img-responsive" src="<?php print thumbnail('', '270'); ?>" width="270" height="270" alt="">
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </section>

</div>

<?php include template_dir() . "footer.php"; ?>
