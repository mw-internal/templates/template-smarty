<?php

/*

  type: layout
  content_type: static
  name: Home 4
  position: 11
  description: Home layout

*/

?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <module type="layouts" template="skin-6"/>
    <module type="layouts" template="skin-7"/>
    <module type="layouts" template="skin-8"/>
    <module type="layouts" template="skin-14"/>
    <module type="layouts" template="skin-21"/>
    <module type="layouts" template="skin-22"/>
    <module type="layouts" template="skin-23"/>
</div>

<?php include template_dir() . "footer.php"; ?>
