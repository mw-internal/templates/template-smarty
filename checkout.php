<?php
/*
type: layout
name: Checkout
position: 3
description: Checkout
*/
?>

<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
    <?php if ($header_style == '' OR $header_style == 'clean'): ?>
        <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
        if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
            <div class="container">
                <h1 class="edit" field="title" rel="content">SHOPPING CART</h1>

                <module type="breadcrumb" id="checkout-breadcrumbs-<?php print PAGE_ID; ?>"/>
            </div>
        </section>
    <?php elseif ($header_style == 'background'): ?>
        <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
            <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

            <div class="container">
                <h1 class="edit" field="title" rel="content">SHOPPING CART</h1>
                <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                <module type="breadcrumb" id="checkout-breadcrumbs-<?php print PAGE_ID; ?>"/>
            </div>
        </section>
    <?php endif; ?>

    <section>
        <div class="container">
            <div field="checkout_page" rel="content">
                <module type="shop/checkout" id="cart_checkout"/>
            </div>
        </div>
    </section>
</div>
<?php include template_dir() . "footer.php"; ?>


