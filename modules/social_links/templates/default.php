<?php

/*

type: layout

name: Default

description: Default

*/
?>

<style>
    .social-links a {
        font-size: 23px;
        padding: 0 5px;
    }

    .social-links a .fa-facebook-official {
        color: #3b5998;
    }

    .social-links a .fa-twitter {
        color: #0084b4;
    }

    .social-links a .fa-google-plus {
        color: #d34836;
    }

    .social-links a .fa-pinterest-p {
        color: #c8232c;
    }

    .social-links a .fa-youtube {
        color: #e52d27;
    }

    .social-links a .fa-instagram {
        color: #125688;
    }

    .social-links a .fa-linkedin {
        color: #007bb5;
    }
</style>

<div class="clearfix">

    <?php if ($social_links_has_enabled == false) {
        print lnotif('Social links');
    } ?>










    <?php if ($facebook_enabled) { ?>
        <a href="//facebook.com/<?php print $facebook_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-facebook pull-left" data-toggle="tooltip"
           data-placement="top" title="Facebook">
            <i class="icon-facebook"></i>
            <i class="icon-facebook"></i>
        </a>
    <?php } ?>

    <?php if ($twitter_enabled) { ?>
        <a href="//twitter.com/<?php print $twitter_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top"
           title="Twitter">
            <i class="icon-twitter"></i>
            <i class="icon-twitter"></i>
        </a>
    <?php } ?>

    <?php if ($googleplus_enabled) { ?>
        <a href="//plus.google.com/<?php print $googleplus_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-gplus pull-left" data-toggle="tooltip"
           data-placement="top" title="Google plus">
            <i class="icon-gplus"></i>
            <i class="icon-gplus"></i>
        </a>
    <?php } ?>

    <?php if ($pinterest_enabled) { ?>
        <a href="//pinterest.com/<?php print $pinterest_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-pinterest pull-left" data-toggle="tooltip"
           data-placement="top" title="Pinterest">
            <i class="fa fa-pinterest-p"></i>
            <i class="fa fa-pinterest-p"></i>
        </a>
    <?php } ?>

    <?php if ($youtube_enabled) { ?>
        <a href="//youtube.com/<?php print $youtube_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-youtube pull-left" data-toggle="tooltip" data-placement="top"
           title="YouTube">
            <i class="fa fa-youtube"></i>
            <i class="fa fa-youtube"></i>
        </a>
    <?php } ?>

    <?php if ($instagram_enabled) { ?>
        <a href="https://instagram.com/<?php print $instagram_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-instagram pull-left" data-toggle="tooltip"
           data-placement="top" title="Instagram">
            <i class="fa fa-instagram"></i>
            <i class="fa fa-instagram"></i>
        </a>
    <?php } ?>

    <?php if ($linkedin_enabled) { ?>
        <a href="//linkedin.com/<?php print $linkedin_url; ?>" target="_blank" class="social-icon social-icon-sm social-icon-border social-linkedin pull-left" data-toggle="tooltip"
           data-placement="top" title="Linkedin">
            <i class="icon-linkedin"></i>
            <i class="icon-linkedin"></i>
        </a>
    <?php } ?>

    <?php if ($rss_enabled) { ?>
        <a href="<?php site_url(); ?>rss" target="_blank" class="social-icon social-icon-sm social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="Rss">
            <i class="icon-rss"></i>
            <i class="icon-rss"></i>
        </a>
    <?php } ?>

</div>