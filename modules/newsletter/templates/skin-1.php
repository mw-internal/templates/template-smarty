<style>
    .alert{
        border-color: transparent !important;
        margin-top: 10px;
    }
    #<?php print $params['id']; ?> .alert li{
        list-style-type: none;
                                   }
</style>
<form method="post" id="newsletters-form-<?php print $params['id'] ?>">
    <?php print csrf_form(); ?>
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
        <input type="email" id="email" name="email" class="form-control required" placeholder="<?php _lang('Enter your Email', "templates/smarty"); ?>">
        <span class="input-group-btn">
            <button class="btn btn-success" type="submit"><?php _lang('Subscribe', "templates/smarty"); ?></button>
        </span>
    </div>
</form>