<?php

/*

type: layout

name: Default

description: List Navigation

*/

?>

<?php
$params['ul_class'] = 'list-group list-group-bordered list-group-noicon uppercase';
$params['ul_class_deep'] = '';
$params['li_class'] = 'list-group-item';
$params['li_class_deep'] = '';


?>



<div class="categories-list">
    <?php category_tree($params); ?>
</div>