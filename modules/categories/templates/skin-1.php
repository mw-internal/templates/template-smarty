<?php

/*

type: layout

name: Portfolio - Categories

description: Skin 1

*/

?>

<?php
$params['ul_class'] = 'page-header-tabs list-inline mix-filter';
$params['ul_class_deep'] = '';
$params['li_class'] = 'filter';
$params['li_class_deep'] = '';
$params['max_level'] = '1';
$params['link'] = '<a href="#">{title}</a>';
?>

<div class="portfolio-categories">
    <?php category_tree($params); ?>
</div>

<script>
    $(document).ready(function () {
        $('.portfolio-categories ul').prepend('<li data-filter="all" class="filter active"><a href="#">All</a></li>');
    });
</script>