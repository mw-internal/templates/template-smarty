<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="row">
    <?php if (isset($data) and !empty($data)): ?>
        <?php
        $count = 0;
        foreach ($data as $slide) {
            $count++;
            ?>
            <div class="col-md-3 col-sm-6" style="margin-bottom:25px;">

                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                    <div class="front">
                        <div class="box1 box-default">
                            <div class="box-icon-title">
                                <?php if ($slide['file']): ?>
                                    <img class="img-responsive" src="<?php print thumbnail($slide['file'], 460); ?>" alt=""/>
                                <?php endif; ?>

                                <h2><?php print array_get($slide, 'name'); ?></h2>
                                <small><?php print array_get($slide, 'role'); ?></small>
                            </div>
                        </div>
                    </div>

                    <div class="back">
                        <div class="box2 box-default">
                            <h4 class="nomargin"><?php print array_get($slide, 'name'); ?></h4>
                            <small><?php print array_get($slide, 'role'); ?></small>

                            <hr/>

                            <p><?php print array_get($slide, 'bio'); ?></p>

                            <hr/>

                            <module type="social_links" id="team_card_<?php print $params['id']; ?>_<?php print md5(array_get($slide, 'name')); ?>"/>
                        </div>
                    </div>
                </div>

            </div>
        <?php } ?>
    <?php endif; ?>
</div>
 
