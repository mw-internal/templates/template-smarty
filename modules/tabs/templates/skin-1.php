<?php

/*

type: layout

name: Tabs skin 1

description: Skin 1

*/

?>


<?php

if ($json == false) {
    print lnotif(_e('Click to edit tabs', true));

    return;
}

if (isset($json) == false or count($json) == 0) {
    $json = array(0 => $defaults);
}

?>

<div id="mw-tabs-module-<?php print $params['id'] ?>" class="tabs">
    <ul class="nav nav-tabs nav-clean">
        <?php
        $count = 0;
        foreach ($json as $slide) {
            $count++;
            ?>
            <li class="<?php if ($count == 1) { ?> active <?php } ?>">
                <a href="#tab-<?php print $count; ?>" data-toggle="tab">
                    <?php print isset($slide['icon']) ? $slide['icon'] . ' ' : ''; ?><?php print isset($slide['title']) ? $slide['title'] : ''; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
    <div class="tab-content margin-bottom-60 margin-top-30">
        <?php
        $count = 0;
        foreach ($json as $slide) {
            $count++;
            ?>
            <div id="tab-<?php print $count; ?>" class="tab-pane fade <?php if ($count == 1) { ?>in active <?php } ?>">
                <div class="edit allow-drop"
                     field="tab-item-<?php print $count ?>"
                     rel="module-<?php print $params['id'] ?>">
                    <div class="element"> <?php print isset($slide['content']) ? $slide['content'] : 'Tab content ' . $count ?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
