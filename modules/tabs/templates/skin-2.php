<?php

/*

type: layout

name: Home accordion

description: Skin 1

*/
?>

<?php

if ($json == false) {
    print lnotif(_e('Click to edit tabs', true));

    return;
}

if (isset($json) == false or count($json) == 0) {
    $json = false;
}

?>

<div id="mw-tabs-module-<?php print $params['id'] ?>" class="toggle toggle-accordion toggle-transparent toggle-bordered-full">
    <?php if ($json): ?>
        <?php foreach ($json as $key => $slide): ?>
            <?php
            if ($key == 0) {
                $helperClass = 'active';
            } else {
                $helperClass = '';
            }
            ?>
            <div class="toggle <?php print $helperClass; ?>">
                <label><?php print isset($slide['title']) ? $slide['title'] : ''; ?></label>
                <div class="toggle-content">
                    <div class="edit allow-drop"
                         field="tab-item-<?php print $key ?>"
                         rel="module-<?php print $params['id'] ?>">
                        <div class="element"> <?php print isset($slide['content']) ? $slide['content'] : 'Tab content ' . $key ?></div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>