<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="toggle toggle-transparent toggle-bordered-simple">
    <?php
    $count = 0;
    foreach ($data as $slide) {
        $count++;
        ?>
        <div class="toggle mix design"><!-- toggle -->
            <label><?php print $count . '. ' . $slide['question']; ?></label>
            <div class="toggle-content">
                <div class="clearfix"></div>
                <?php print $slide['answer']; ?>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php } ?>
</div>

 
