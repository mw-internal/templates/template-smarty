<?php

/*

type: layout

name: Default

description: Default


*/

?>
<?php if (isset($data) and is_array($data)): ?>
    <!-- breadcrumbs -->
    <ol class="breadcrumb">
        <?php foreach ($data as $item): ?>
            <?php if (!($item['is_active'])): ?>
                <li><a href="<?php print($item['url']); ?>"><?php print($item['title']); ?></a></li>
            <?php else: ?>
                <li class="active"><?php print($item['title']); ?> </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ol><!-- /breadcrumbs -->
<?php endif; ?>