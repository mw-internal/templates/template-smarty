<?php

/*

type: layout

name: Default

description: Default

*/

?>


<?php if ($items): ?>
    <div class="twitter-feed-default">
        <ul class="hidden-xs widget-twitter margin-bottom-60">
            <?php foreach ($items as $tweet): ?>
                <li><i class="fa fa-twitter"></i><span>
                        <?php

                        $tweetText = $tweet['text'];
                        $hashPattern = '/\#([A-Za-z0-9\_]+)/i';
                        $mentionPattern = '/\@([A-Za-z0-9\_]+)/i';
                        $urlPattern = '/(http[s]?\:\/\/[^\s]+)/i';
                        $robotsFollow = false;

                        $tweetText = preg_replace($urlPattern, '<a href="$1" rel="nofollow"' . '>$1</a>', $tweetText);
                        $tweetText = preg_replace($hashPattern, '<a href="http://twitter.com/hashtag/$1" >#$1</a>', $tweetText);
                        $tweetText = preg_replace($mentionPattern, '<a href="http://twitter.com/$1">@$1</a>', $tweetText);

                        print ($tweetText); ?>
                    </span>
                    <small><a href="<?php print $tweet['url']; ?>" target="_blank"><?php print $tweet['ago']; ?></a></small>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
