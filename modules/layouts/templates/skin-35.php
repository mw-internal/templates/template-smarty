<?php

/*

type: layout

name: Happy clients

position: 35

*/
?>

<section class="padding-xs edit nodrop safe-mode" field="layout-skin-35-<?php print $params['id'] ?>" rel="module">
    <div class="container">

        <header class="text-center margin-bottom-60">
            <h4>HAPPY CLIENTS</h4>
        </header>

        <module type="pictures" template="skin-3"/>
    </div>
</section>