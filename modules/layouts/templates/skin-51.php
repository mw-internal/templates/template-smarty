<?php

/*

type: layout

name: Left Vimeo Video - Right Text

position: 50

*/
?>

<?php
$vimeo_code = get_option('vimeo_code', $params['id']);
if ($vimeo_code === null OR $vimeo_code === false OR $vimeo_code == '') {
    $vimeo_code = '8408210';
}
?>

<section class="edit nodrop" field="layout-skin-51-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="embed-responsive embed-responsive-16by9 box-shadow-1">
                    <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/<?php print $vimeo_code; ?>" width="800" height="450"></iframe>
                </div>
            </div>
            <div class="col-lg-6 allow-drop">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam in minima iusto voluptatem aliquam odit odio. Aliquam voluptatibus beatae officiis?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique excepturi voluptates placeat ducimus delectus magnam tempore dolore dolorem quisquam porro modi voluptatum eum saepe dolorum ratione officia sint eos minus.</p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc.</p>
                    <cite>Albert Einstein</cite>
                </blockquote>
            </div>
        </div>
    </div>
</section>