<?php

/*

type: layout

name: Team Card With Background

position: 18

*/

$overlay = get_option('overlay', $params['id']);

if ($overlay == false) {
    $overlay = '5';
}
?>

<section class="parallax parallax-2 edit nodrop safe-mode" style="background-image: url('<?php print template_url('assets/images/'); ?>hero.jpg');" field="layout-skin-19-<?php print $params['id'] ?>" rel="module">
    <div class="overlay dark-<?php print $overlay; ?>"></div>
    <div class="container">
        <module type="teamcard"/>
    </div>
</section>
