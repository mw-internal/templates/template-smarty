<?php

/*

type: layout

name: Heading

position: 46

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-46-<?php print $params['id'] ?>" rel="module">>
    <div class="container">
        <div class="text-center allow-drop">
            <h1>Welcome! World Of <span>Smarty</span>.</h1>
            <h2 class="col-sm-10 col-sm-offset-1 nomargin-bottom weight-400">Clean, fully responsive, extemly flexible multipurpose template that allows you to create any website you like.</h2>
        </div>
    </div>
</section>