<?php

/*

type: layout

name: Tabs & Skills

position: 18

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-18-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="heading-title heading-border-bottom">
                    <h3>Smarty Services</h3>
                </div>

                <module type="tabs" template="skin-1"/>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="heading-title heading-border-bottom">
                    <h3>Smarty Skills</h3>
                </div>

                <module type="skills" template="skin-1"/>
            </div>
        </div>
    </div>
</section>
