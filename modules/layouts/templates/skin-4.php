<?php

/*

type: layout

name: Products in Row

position: 4

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-4-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="heading-title heading-dotted">
            <h2 class="size-20">NEW PRODUCTS</h2>
        </div>

        <module type="shop/products" template="default"/>
    </div>
</section>