<?php

/*

type: layout

name: Title

position: 25

*/
?>


<section class="page-header edit nodrop safe-mode page-header-xs"  field="layout-skin-25-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <h1>Title</h1>

        <module type="breadcrumb" id="services-1-breadcrumbs-<?php print PAGE_ID; ?>"/>
    </div>
</section>