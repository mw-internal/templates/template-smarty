<div id="settings-holder">
    <br/><br/>
    <h5 style="font-weight: bold; display: block;">Skin Settings</h5>

    <?php
    $overlay = get_option('overlay', $params['id']);
    if ($overlay == '') {
        $overlay = '';
    }
    ?>

    <div class="overlay" style="margin-top:15px;">
        <label class="mw-ui-label">Overlay</label>
        <input type="text" name="overlay" placeholder="from 0 to 9" class="mw-ui-field mw_option_field"/>
    </div>
</div>