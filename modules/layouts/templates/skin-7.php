<?php

/*

type: layout

name: Left Image - Right Text

position: 7

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-7-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <img class="img-responsive image-hover thumbnail" src="<?php print template_url('assets/images/placeholder.jpg') ?>" alt="...">
            </div>

            <div class="col-md-7 col-sm-7">
                <header class="margin-bottom-60">
                    <h2>Smarty Website</h2>
                    <p class="lead font-lato">The most complete template, ever!</p>
                </header>

                <div class="allow-drop">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus deserunt, nobis quae eos provident quidem. Quaerat expedita dignissimos perferendis, nihil quo distinctio
                        eius architecto reprehenderit maiores.</p>
                    <p>Similique excepturi voluptates placeat ducimus delectus magnam tempore dolore dolorem quisquam porro modi voluptatum eum saepe dolorum ratione officia sint eos minus.</p>

                    <module type="btn" button_style="btn-default" button_size="btn-md" text="WATCH VIDEO"/>
                </div>

            </div>
        </div>
    </div>
</section>