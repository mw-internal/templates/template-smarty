<?php

/*

type: layout

name: Left Slider - Right Text

position: 17

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-17-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <module type="bxslider" template="owl-2"/>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="heading-title heading-border-bottom">
                    <h3>We believe in Simple &amp; Creative</h3>
                </div>
                <div class="allow-drop">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique excepturi voluptates placeat ducimus delectus magnam tempore dolore dolorem quisquam porro modi
                        voluptatum eum saepe dolorum.</p>
                    <blockquote class="safe-element">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc.</p>
                        <cite>Albert Einstein</cite>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</section>