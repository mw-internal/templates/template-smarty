<?php

/*

type: layout

name: Simple Heading

position: 47

*/
?>


<div class="edit clean-container nodrop" field="layout-skin-47-<?php print $params['id'] ?>" rel="module">
    <div class="container allow-drop">
        <div class="heading-title">
            <h3>Simple Heading</h3>
        </div>
    </div>
</div>