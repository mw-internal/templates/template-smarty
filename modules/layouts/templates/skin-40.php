<?php

/*

type: layout

name: Text boxes + Icons 3

position: 40

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-40-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="allow-drop">
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget
                diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin
                adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.</p>
        </div>

        <div class="divider divider-center divider-color">
            <i class="fa fa-chevron-down safe-element"></i>
        </div>

        <div class="row">

            <div class="col-md-4 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-color ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Web Design</h4>
                    <p class="font-lato size-20">Donec id elit non mi porta gravida.</p>
                </div>
            </div>

            <div class="col-md-4 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-color ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Web Analytics</h4>
                    <p class="font-lato size-20">Donec id elit non mi porta gravida.</p>
                </div>
            </div>

            <div class="col-md-4 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-color ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Development</h4>
                    <p class="font-lato size-20">Donec id elit non mi porta gravida.</p>
                </div>
            </div>

            <div class="col-md-4 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-color ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Marketing</h4>
                    <p class="font-lato size-20">Donec id elit non mi porta gravida.</p>
                </div>
            </div>

            <div class="col-md-4 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-color ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Sales Booster</h4>
                    <p class="font-lato size-20">Donec id elit non mi porta gravida.</p>
                </div>
            </div>

            <div class="col-md-4 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-color ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Design</h4>
                    <p class="font-lato size-20">Donec id elit non mi porta gravida.</p>
                </div>
            </div>

        </div>
    </div>
</section>
