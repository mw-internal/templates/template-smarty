<div id="settings-holder">
    <br/><br/>
    <h5 style="font-weight: bold; display: block;">Skin Settings</h5>

    <?php
    $style = get_option('style', $params['id']);
    if ($style === null OR $style === false OR $style == '' OR $style == 'btn-default') {
        $style = 'btn-default';
    }

    $url = get_option('url', $params['id']);
    if ($url === null OR $url === false OR $url == '') {
        $url = '';
    }

    $text = get_option('text', $params['id']);
    if ($text === null OR $text === false OR $text == '') {
        $text = '';
    }
    ?>

    <div class="style-select" style="padding-top: 15px;">
        <label class="mw-ui-label">Style</label>
        <select name="style" class="mw-ui-field mw_option_field" data-option-group="<?php print $params['id']; ?>">
            <option value="" <?php if ($style == 'btn-default') {
                echo 'selected';
            } ?>>No Selected
            </option>

            <option value="btn-primary"<?php if ($style == 'btn-primary') {
                echo 'selected';
            } ?>>Primary
            </option>

            <option value="btn-warning"<?php if ($style == 'btn-warning') {
                echo 'selected';
            } ?>>Warning
            </option>

            <option value="btn-danger"<?php if ($style == 'btn-danger') {
                echo 'selected';
            } ?>>Danger
            </option>

            <option value="btn-success"<?php if ($style == 'btn-success') {
                echo 'selected';
            } ?>>Success
            </option>

            <option value="btn-info"<?php if ($style == 'btn-info') {
                echo 'selected';
            } ?>>Info
            </option>
        </select>
    </div>

    <div class="url-select" style="padding-top: 15px;">
        <label class="mw-ui-label">URL</label>
        <input type="text" name="url" placeholder="Button URL" value="<?php print $url; ?>" class="mw-ui-field mw_option_field"/>
    </div>

    <div class="text-select" style="padding-top: 15px;">
        <label class="mw-ui-label">Text</label>
        <input type="text" name="text" placeholder="Button text" value="<?php print $text; ?>" class="mw-ui-field mw_option_field"/>
    </div>
</div>