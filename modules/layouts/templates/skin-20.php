<?php

/*

type: layout

name: Pictures Gallery with TItle

position: 20

*/
?>

<section class="padding-xs edit nodrop safe-mode" field="layout-skin-20-<?php print $params['id'] ?>" rel="module">
    <div class="container">

        <header class="text-center margin-bottom-10">
            <h3>HAPPY CLIENTS</h3>
        </header>

        <hr class="margin-bottom-60"/>

        <module type="pictures"/>
    </div>
</section>