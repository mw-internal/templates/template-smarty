<?php

/*

type: layout

name: Three columns Icons with text

position: 57

*/
?>

<style>
    #
    <?php print $params['id'] ?>
    i {
        font-size: 70px;
    }
</style>

<section class="edit nodrop safe-mode text-center" field="layout-skin-57-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 cloneable">
                <i class="fa fa-info-circle fa-5x safe-element"></i>
                <h2 class="size-17 margin-top-6">WHY DREAM IS DOWN?</h2>
                <div class="allow-drop">
                    <p class="text-muted">We are working hard to migrate the data to a new datacenter. We'll be back
                        online soon as possible, we really apologize for this inconvenience.</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 cloneable">
                <i class="fa fa-clock-o fa-5x safe-element"></i>
                <h2 class="size-17 margin-top-6">WHAT IS THE DOWNTIME?</h2>
                <div class="allow-drop">
                    <p class="text-muted">Unfortunately we do not know the exact downtime, but we esitmate less than an
                        hour.</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 cloneable">
                <i class="fa fa-comments-o fa-5x safe-element"></i>
                <h2 class="size-17 margin-top-6">SUPPORT</h2>
                <div class="allow-drop">
                    <p class="text-muted">If you have questions, please do not hesitate to write us an email at <a
                                href="mailto:support@domaain.com">support@domaain.com</a></p>
                </div>
            </div>
        </div>
    </div>
</section>