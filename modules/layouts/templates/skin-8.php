<?php

/*

type: layout

name: Text with Icons

position: 8

*/

$overlay = get_option('overlay', $params['id']);

if ($overlay == false) {
    $overlay = '4';
}
?>

<section class="parallax parallax-2 padding-xxs edit nodrop safe-mode" style="background-image: url('<?php print template_url('assets/images/'); ?>hero.jpg');" field="layout-skin-8-<?php print $params['id'] ?>"
         rel="module">
    <div class="overlay dark-<?php print $overlay; ?>"></div>

    <div class="container">
        <div class="row countTo-md text-center">
            <div class="col-xs-6 col-sm-3 cloneable">
                <i class="fa fa-users safe-element"></i>
                <span class="countTo safe-element" data-speed="3000">1303</span>
                <h5>HAPPY CLIENTS</h5>
            </div>

            <div class="col-xs-6 col-sm-3 cloneable">
                <i class="fa fa-code safe-element"></i>
                <span class="countTo safe-element" data-speed="3000">56000</span>
                <h5>LINES OF CODE</h5>
            </div>

            <div class="col-xs-6 col-sm-3 cloneable">
                <i class="fa fa-twitter safe-element"></i>
                <span class="countTo safe-element" data-speed="3000">4897</span>
                <h5>TWITTER</h5>
            </div>

            <div class="col-xs-6 col-sm-3 cloneable">
                <i class="fa fa-facebook safe-element"></i>
                <span class="countTo safe-element" data-speed="3000">9877</span>
                <h5>FACEBOOK</h5>
            </div>
        </div>
    </div>
</section>