<?php

/*

type: layout

name: List with Icons

position: 37

*/
?>


<section class="edit nodrop safe-mode" field="layout-skin-37-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="fa fa-tablet safe-element"></i>
                        <h2>FULLY REPOSNIVE</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-document safe-element"></i>
                        <h2>RTL SUPPORT</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-basket safe-element"></i>
                        <h2>MOBILE COMPATIBILE</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-briefcase safe-element"></i>
                        <h2>CLEAN CODE</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-megaphone safe-element"></i>
                        <h2>PREMIUM SLIDERS</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-flag safe-element"></i>
                        <h2>SIMPLE TO USE</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-puzzle safe-element"></i>
                        <h2>LIGHTWEIGHT</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-trophy safe-element"></i>
                        <h2>ADMIN INCLUDED</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 cloneable">
                <div class="box-icon box-icon-left">
                    <a class="box-icon-title safe-element" href="javascript:;">
                        <i class="et-layers safe-element"></i>
                        <h2>PARALLAX</h2>
                    </a>
                    <p class="text-muted">Lorem definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii.</p>
                </div>
            </div>

        </div>
    </div>
</section>