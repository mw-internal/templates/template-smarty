<?php

/*

type: layout

name: Text boxes + Icons

position: 38

*/
?>

<section class="alternate edit nodrop safe-mode" field="layout-skin-38-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">

            <div class="col-md-4 cloneable">
                <div class="box-icon box-icon-side box-icon-color box-icon-round">
                    <i class="fa fa-tablet safe-element"></i>
                    <a class="box-icon-title safe-element" href="#">
                        <h2>Fully Reposnive</h2>
                    </a>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                    <a class="box-icon-more font-lato weight-300 safe-element" href="#">Learn More</a>
                </div>
            </div>

            <div class="col-md-4 cloneable">
                <div class="box-icon box-icon-side box-icon-color box-icon-round">
                    <i class="fa fa-tint safe-element"></i>
                    <a class="box-icon-title safe-element" href="#">
                        <h2>Reusable Elements</h2>
                    </a>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                    <a class="box-icon-more font-lato weight-300 safe-element" href="#">Learn More</a>
                </div>
            </div>

            <div class="col-md-4 cloneable">
                <div class="box-icon box-icon-side box-icon-color box-icon-round">
                    <i class="fa fa-cogs safe-element"></i>
                    <a class="box-icon-title safe-element" href="#">
                        <h2>Multipurpose</h2>
                    </a>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                    <a class="box-icon-more font-lato weight-300 safe-element" href="#">Learn More</a>
                </div>
            </div>

        </div>
    </div>
</section>
