<?php

/*

type: layout

name: Goodies

position: 14

*/
?>


<section class="edit nodrop safe-mode" field="layout-skin-14-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-3 cloneable">
                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title safe-element" href="#">
                        <i class="fa fa-tablet safe-element"></i>
                        <h2>Fully Reposnive</h2>
                    </a>
                    <p class="text-muted">Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante.</p>
                </div>
            </div>

            <div class="col-md-3 cloneable">
                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title safe-element" href="#">
                        <i class="fa fa-random safe-element"></i>
                        <h2>Clean Design</h2>
                    </a>
                    <p class="text-muted">Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante.</p>
                </div>
            </div>

            <div class="col-md-3 cloneable">
                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title safe-element" href="#">
                        <i class="fa fa-tint safe-element"></i>
                        <h2>Reusable Elements</h2>
                    </a>
                    <p class="text-muted">Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante.</p>
                </div>
            </div>

            <div class="col-md-3 cloneable">
                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title safe-element" href="#">
                        <i class="fa fa-cogs safe-element"></i>
                        <h2>Multipurpose</h2>
                    </a>
                    <p class="text-muted">Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante.</p>
                </div>
            </div>
        </div>
    </div>
</section>