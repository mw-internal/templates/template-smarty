<?php

/*

type: layout

name: Alert Black & White

position: 22

*/
?>


<div class="edit nodrop safe-mode" field="layout-skin-22-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="callout alert alert-border margin-top-60">
            <div class="row">
                <div class="col-md-8 col-sm-8 allow-drop">
                    <h4>Call now at <strong>+800-565-2390</strong> and get 15% discount!</h4>
                    <p class="font-lato size-17">
                        We truly care about our users and our product.
                    </p>
                </div>

                <div class="col-md-4 col-sm-4 text-right">
                    <module type="btn" button_style="btn-default" button_size="btn-md" text="PURCHASE NOW"/>
                </div>
            </div>
        </div>
    </div>
</div>
