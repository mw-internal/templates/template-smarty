<?php

/*

type: layout

name: Categories

position: 15

*/
?>

<section class="page-header page-header-xs edit nodrop safe-mode" field="layout-skin-15-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <module type="categories" template="skin-1"/>
    </div>
</section>