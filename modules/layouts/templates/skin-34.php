<?php

/*

type: layout

name: Left image + Right text

position: 34

*/
?>

<section id="about" class="edit nodrop safe-mode" field="layout-skin-34-<?php print $params['id'] ?>" rel="module">
    <div class="container">

        <header class="text-center margin-bottom-60 allow-drop">
            <h2>We Are Smarty</h2>
            <p class="lead font-lato">Lorem ipsum dolor sit amet adipiscium elit</p>
            <hr />
        </header>

        <div class="row">
            <div class="col-sm-6">
                <img class="img-responsive" src="<?php print template_url('assets/images/'); ?>demo/laptop.png" alt="" />
            </div>

            <div class="col-sm-6 allow-drop">
                <p class="dropcap">Lorem ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>

                <hr />

                <div class="row countTo-sm text-center nodrop">
                    <div class="col-xs-6 col-sm-4">
                        <i class="fa fa-users size-20 safe-element"></i> &nbsp;
                        <span class="countTo safe-element" data-speed="3000" style="color:#59BA41">1303</span>
                        <h6>HAPPY CLIENTS</h6>
                    </div>

                    <div class="col-xs-6 col-sm-4">
                        <i class="fa fa-briefcase size-20 safe-element"></i> &nbsp;
                        <span class="countTo safe-element" data-speed="3000" style="color:#774F38">56000</span>
                        <h6>FINISHED PROJECTS</h6>
                    </div>

                    <div class="col-xs-6 col-sm-4">
                        <i class="fa fa-twitter size-20 safe-element"></i> &nbsp;
                        <span class="countTo safe-element" data-speed="3000" style="color:#C02942">4897</span>
                        <h6>TWITTER FOLLOWERS</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>