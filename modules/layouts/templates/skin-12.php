<?php

/*

type: layout

name: Full Witdh Search

position: 12

*/
?>


<section class="dark padding-xxs edit nodrop safe-mode" field="layout-skin-12-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <module type="search"/>
    </div>
</section>