<div id="settings-holder">
    <br/><br/>
    <h5 style="font-weight: bold; display: block;">Skin Settings</h5>

    <?php
    $vimeo_code = get_option('vimeo_code', $params['id']);
    if ($vimeo_code === null OR $vimeo_code === false OR $vimeo_code == '') {
        $vimeo_code = '';
    }
    ?>

    <div class="vimeo-video-select" style="padding-top: 15px;">
        <label class="mw-ui-label">Vimeo Code</label>
        <input type="text" name="vimeo_code" placeholder="Vimeo Code" value="<?php print $vimeo_code; ?>" class="mw-ui-field mw_option_field"/>
    </div>
</div>