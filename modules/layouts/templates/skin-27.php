<?php

/*

type: layout

name: Icons with text

position: 27

*/
?>



<section class="edit nodrop safe-mode" field="layout-skin-27-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="fa fa-check ico-light ico-lg ico-rounded ico-hover safe-element"></i>
                    <h4>Web Design</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>

                    <a href="#" class="safe-element">
                        Read more <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>

            <div class="col-md-3 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-search safe-element"></i>
                    <h4>Web Analytics</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>

                    <a href="#" class="safe-element">
                        Read more <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>

            <div class="col-md-3 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-angle-right safe-element"></i>
                    <h4>Web Development</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>

                    <a href="#" class="safe-element">
                        Read more <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>

            <div class="col-md-3 col-xs-6 cloneable">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-at safe-element"></i>
                    <h4>Marketing</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus. </p>

                    <a href="#" class="safe-element">
                        Read more <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>