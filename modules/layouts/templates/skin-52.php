<?php

/*

type: layout

name: Two Videos in Row With Text

position: 52

*/
?>

<section class="edit nodrop" field="layout-skin-52-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-6 allow-drop">
                <module type="video"/>
                <h4><b>Vimeo</b> Video</h4>
                <p>Some video description</p>
            </div>
            <div class="col-md-6 allow-drop">
                <module type="video"/>
                <h4><b>Youtube</b> Video</h4>
                <p>Some video description</p>
            </div>
        </div>
    </div>
</section>