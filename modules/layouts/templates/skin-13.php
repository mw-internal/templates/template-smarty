<?php

/*

type: layout

name: Posts

position: 13

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-13-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <module type="posts" template="skin-4"/>
    </div>
</section>