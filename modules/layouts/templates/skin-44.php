<?php

/*

type: layout

name: Steps

position: 44

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-44-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="heading-title text-center margin-bottom-80">
            <h3>Steps</h3>
        </div>

        <ul class="process-steps nav nav-justified">
            <li class="cloneable">
                <a href="javascript:;" class="safe-element"><i class="fa fa-tint"></i></a>
                <h5>Step 1</h5>
            </li>
            <li class="active cloneable">
                <a href="javascript:;" class="safe-element"><i class="fa fa-search"></i></a>
                <h5>Step 2</h5>
            </li>
            <li class="cloneable">
                <a href="javascript:;" class="safe-element"><i class="fa fa-envelope-open"></i></a>
                <h5>Step 3</h5>
            </li>

            <li class="active cloneable">
                <a href="javascript:;" class="safe-element"><i class="fa fa-address-card"></i></a>
                <h5>Finish</h5>
            </li>
        </ul>
    </div>
</section>