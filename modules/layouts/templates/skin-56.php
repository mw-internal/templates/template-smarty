<?php

/*

type: layout

name: Pricing Table

position: 56

*/
?>

<style>
    @media screen and (max-width: 768px) {
    #<?php print $params['id'] ?> .mw-ui-col {
                                      margin-bottom: 15px !important;
                                  }
    }
</style>

<section class="edit nodrop safe-mode" field="layout-skin-56-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="heading-title heading-line-single text-center">
            <h3>Pricing Table</h3>
        </div>

        <div class="mw-ui-row mega-price-table">
            <div class="mw-ui-col cloneable">
                <div class="pricing">
                    <div class="pricing-head">
                        <h3>Basic</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        9<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            20 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            100 GB <span class="hidden-md hidden-lg safe-element">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            10 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>

            <div class="mw-ui-col cloneable">
                <div class="pricing popular">

                    <div class="pricing-head">
                        <h3>Standard</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        19<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            500 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            300 GB <span class="hidden-md hidden-lg safe-element">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            200 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>

            <div class="mw-ui-col cloneable">
                <div class="pricing">

                    <div class="pricing-head">
                        <h3>Super</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        49<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            1000 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            800 GB <span class="hidden-md hidden-lg safe-element">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            300 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>

            <div class="mw-ui-col cloneable">
                <div class="pricing">

                    <div class="pricing-head">
                        <h3>Ultimate</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        99<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            1500 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            1000 GB <span class="hidden-md hidden-lg">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            100 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</section>