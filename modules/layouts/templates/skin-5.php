<?php

/*

type: layout

name: Images in Slider - Brands

position: 5

*/
?>

<section class="padding-xxs edit nodrop safe-mode" field="layout-skin-5-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <module type="pictures" template="default"/>
    </div>
</section>