<?php

/*

type: layout

name: Clients Cards

position: 43

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-43-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="cloneable">
            <div class="row">
                <div class="col-md-2">
                    <img src="<?php print template_url('assets/images/'); ?>demo/brands/1.jpg" class="img-responsive safe-element" alt="">
                </div>
                <div class="col-md-10 allow-drop">
                    <h4 class="margin-bottom-10">Company Name, Inc</h4>
                    <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
                        occaecati</p>
                </div>
            </div>

            <hr/>
        </div>

        <div class="cloneable">
            <div class="row">
                <div class="col-md-2">
                    <img src="<?php print template_url('assets/images/'); ?>demo/brands/2.jpg" class="img-responsive safe-element" alt="">
                </div>
                <div class="col-md-10 allow-drop">
                    <h4 class="margin-bottom-10">Company Name, Inc</h4>
                    <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
                        occaecati</p>
                </div>
            </div>

            <hr/>
        </div>
    </div>
</section>