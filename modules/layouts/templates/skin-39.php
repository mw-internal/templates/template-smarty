<?php

/*

type: layout

name: Text boxes + Icons 2

position: 39

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-39-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-6 cloneable">
                <div class="box-icon box-icon-center box-icon-round box-icon-transparent box-icon-large box-icon-content">
                    <div class="box-icon-title">
                        <i class="fa fa-tablet safe-element"></i>
                        <h2>Fully Reposnive</h2>
                    </div>
                    <div class="allow-drop">
                        <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis
                            dapibus posuere</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 cloneable">
                <div class="box-icon box-icon-center box-icon-round box-icon-transparent box-icon-large box-icon-content">
                    <div class="box-icon-title">
                        <i class="fa fa-random safe-element"></i>
                        <h2>Clean Design</h2>
                    </div>
                    <div class="allow-drop">
                        <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis
                            dapibus posuere</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
