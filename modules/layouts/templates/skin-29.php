<?php

/*

type: layout

name: Slider + Text

position: 29

*/
?>


<section class="edit nodrop safe-mode" field="layout-skin-29-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="allow-drop">
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget
                diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin
                adipiscing
                porta tellus, ut feugiat nibh adipiscing sit amet.</p>
        </div>

        <div class="divider divider-center divider-color">
            <i class="fa fa-chevron-down safe-element"></i>
        </div>

        <article class="row">
            <div class="col-md-6">
                <module type="pictures" template="skin-1"/>
            </div>
            <div class="col-md-6 allow-drop">
                <h3>Smarty : Four Areas of Expertise</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam.</p>

                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                    magni dolores eos qui ratione voluptatem.</p>

                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>

                <p>Enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                    dolores eos qui ratione voluptatem sequi nesciunt.</p>
            </div>
        </article>
    </div>
</section>