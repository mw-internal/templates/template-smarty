<?php

/*

type: layout

name: Portfolio Posts

position: 16

*/
?>

<section class="nopadding edit nodrop safe-mode" field="layout-skin-16-<?php print $params['id'] ?>" rel="module">
    <module type="posts" template="skin-2" nolimit="true"/>
</section>