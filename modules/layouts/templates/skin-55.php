<?php

/*

type: layout

name: Pricing Table

position: 55

*/
?>

<style>
    @media screen and (max-width: 991px) {
        #<?php print $params['id'] ?> .mw-ui-col {
            display: block !important;
            float: left;
            width: 50%;
            margin-bottom: 10px;
        }
    }

    @media screen and (max-width: 560px) {
        #<?php print $params['id'] ?> .mw-ui-col {
            display: block !important;
            float: left;
            width: 100%;
            margin-bottom: 10px;
        }
    }
</style>

<section class="edit nodrop safe-mode" field="layout-skin-55-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="heading-title heading-line-single text-center">
            <h3>Pricing Table</h3>
        </div>

        <div class="mw-ui-row">
            <div class="mw-ui-col cloneable" style="padding: 0 15px;">
                <div class="price-clean">
                    <h4><sup class="safe-element">$</sup>15<em class="safe-element">/month</em></h4>
                    <h5> STARTER </h5>
                    <hr/>
                    <p>For individuals looking for something simple to get started.</p>
                    <hr/>
                    <module type="btn" text="Learn More" />
                </div>
            </div>

            <div class="mw-ui-col cloneable" style="padding: 0 15px;">
                <div class="price-clean price-clean-popular">
                    <div class="ribbon">
                        <div class="ribbon-inner">POPULAR</div>
                    </div>
                    <h4><sup class="safe-element">$</sup>25<em class="safe-element">/month</em></h4>
                    <h5> BUSINESS </h5>
                    <hr/>
                    <p>For individuals looking for something simple to get started.</p>
                    <hr/>
                    <module type="btn" text="Learn More" />
                </div>
            </div>

            <div class="mw-ui-col cloneable" style="padding: 0 15px;">
                <div class="price-clean">
                    <h4><sup class="safe-element">$</sup>35<em class="safe-element">/month</em></h4>
                    <h5> DEVELOPER </h5>
                    <hr/>
                    <p>For individuals looking for something simple to get started.</p>
                    <hr/>
                    <module type="btn" text="Learn More" />
                </div>
            </div>
        </div>
    </div>
</section>