<?php

/*

type: layout

name: Pricing Table

position: 30

*/
?>

<style>
    @media screen and (max-width: 768px) {
    #<?php print $params['id'] ?> .mw-ui-col.pricing-desc {
        display: none !important;
    }

    #<?php print $params['id'] ?> .mw-ui-col {
                                      margin-bottom: 15px !important;
                                  }
    }
</style>

<section class="edit nodrop safe-mode" field="layout-skin-30-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="heading-title heading-line-single text-center">
            <h3>Pricing Table</h3>
        </div>

        <div class="mw-ui-row mega-price-table">
            <div class="mw-ui-col hidden-sm hidden-xs pricing-desc">
                <div class="pricing-title">
                    <h3>Hosting Plans
                        <small>#2</small>
                    </h3>
                    <p class="text-muted">Lorem ipsum dolor sit amet</p>
                </div>

                <ul class="list-unstyled">
                    <li class="cloneable">Data Storage</li>
                    <li class="alternate cloneable">Monthly Traffic</li>
                    <li class="cloneable">Email Accounts</li>
                    <li class="alternate cloneable">Online Support</li>
                    <li class="cloneable">Premium DNS</li>
                    <li class="alternate cloneable">SSL Certificate</li>
                </ul>
            </div>

            <div class="mw-ui-col cloneable">
                <div class="pricing">
                    <div class="pricing-head">
                        <h3>Basic</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        9<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            20 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            100 GB <span class="hidden-md hidden-lg safe-element">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            10 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>

            <div class="mw-ui-col cloneable">
                <div class="pricing popular">
                    <div class="pricing-head">
                        <h3>Basic</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        9<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            20 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            100 GB <span class="hidden-md hidden-lg safe-element">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            10 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>

            <div class="mw-ui-col cloneable">
                <div class="pricing">
                    <div class="pricing-head">
                        <h3>Basic</h3>
                        <small class="safe-element">per month</small>
                    </div>

                    <h4>
                        <small class="safe-element">$</small>
                        9<sup class="safe-element">.49</sup>
                    </h4>

                    <ul class="pricing-table list-unstyled">
                        <li class="cloneable">
                            20 Gb
                            <span class="hidden-md hidden-lg safe-element">Data Storage</span>
                        </li>
                        <li class="alternate cloneable">
                            100 GB <span class="hidden-md hidden-lg safe-element">Monthly Traffic</span>
                        </li>
                        <li class="cloneable">
                            10 <span class="hidden-md hidden-lg safe-element">Email Accounts</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-check safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Online Support</span>
                        </li>
                        <li class="cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">Premium DNS</span>
                        </li>
                        <li class="alternate cloneable">
                            <i class="fa fa-times safe-element"></i>
                            <span class="hidden-md hidden-lg safe-element">SSL Certificate</span>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary fullwidth safe-element">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</section>