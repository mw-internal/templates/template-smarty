<?php

/*

type: layout

name: Recent Works

position: 21

*/
?>

<div class="edit nodrop safe-mode" field="layout-skin-21-<?php print $params['id'] ?>" rel="module">
    <div class="container">

        <header class="margin-bottom-60 text-center allow-drop">
            <h2>Smarty Recent Works</h2>
            <p class="lead font-lato">We believe in Simple &amp; Creative</p>
            <hr/>
        </header>

        <module type="posts"/>
    </div>
</div>