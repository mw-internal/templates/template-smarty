<?php

/*

type: layout

name: Three Banners

position: 1

*/
?>

<section class="featured-grid nodrop safe-mode edit" field="layout-skin-1-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div style="width: 577px; height: 546px; background-image: url('<?php print thumbnail('', 577, 546, true); ?>'); background-position: center center; background-size: cover; background-repeat: no-repeat;"></div>
                <div class="ribbon hidden-xs">
                    <div class="absolute">
                        <h2>87%</h2>
                        <h4>OFF</h4>
                    </div>
                </div>
                <div class="absolute bottom-right text-right">
                    <h1 class="text-white">
                        <em class="safe-element">HUGE</em>
                        <em class="weight-300 text-white safe-element">DISCOUNTS</em>
                    </h1>

                    <a class="btn btn-primary btn-xs margin-top-10 safe-element" href="#">Explore Now &raquo;</a>
                    <module type="btn" text="Explore Now"/>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="relative">
                    <div style="width: 577px; height: 268px; background-image: url('<?php print thumbnail('', 577, 268, true); ?>'); background-position: center center; background-size: cover; background-repeat: no-repeat;"></div>

                    <div class="absolute text-left">
                        <h2>Men</h2>
                        <p>YOU DON'T WANT TO MISS!</p>

                        <p class="font-lato margin-top-30 size-16 hidden-sm hidden-xs">Donec tellus massa, tristique sit amet condim vel,<br/> facilisis quis sapien. Praesent id enim sit amet.</p>
                        <a class="btn btn-primary margin-top-20 safe-element" href="#">Shop Now &raquo;</a>
                    </div>
                </div>

                <div class="relative">
                    <div style="width: 577px; height: 268px; background-image: url('<?php print thumbnail('', 577, 268, true); ?>'); background-position: center center; background-size: cover; background-repeat: no-repeat;"></div>
                    <div class="absolute text-right">
                        <h2>Women</h2>
                        <p>YOU DON'T WANT TO MISS!</p>

                        <p class="font-lato margin-top-30 size-16 hidden-sm hidden-xs">Donec tellus massa, tristique sit amet condim vel,<br/> facilisis quis sapien. Praesent id enim sit amet.</p>
                        <a class="btn btn-primary margin-top-20 safe-element" href="#">Shop Now &raquo;</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
