<?php

/*

type: layout

name: Text & Slider

position: 9

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-9-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <header class="margin-bottom-40">
                    <h1 class="weight-300">Welcome to Smarty</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>THE MOST COMPLETE TEMPLATE EVER</span></h2>
                </header>

                <div class="allow-drop">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero quod consequuntur quibusdam, enim expedita sed quia nesciunt incidunt accusamus necessitatibus modi adipisci
                        officia libero accusantium esse hic, obcaecati, ullam, laboriosam!</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti vero, animi suscipit id facere officia. Aspernatur, quo, quos nisi dolorum aperiam fugiat deserunt velit rerum
                        laudantium cum magnam.</p>
                </div>
                <hr/>

                <div class="text-center margin-top-30">
                    <module type="pictures" template="default" id="slider<?php print content_id(); ?>"/>
                </div>
            </div>

            <div class="col-sm-6">
                <module type="bxslider" template="owl" id="slider-<?php print content_id(); ?>"/>
            </div>
        </div>
    </div>
</section>