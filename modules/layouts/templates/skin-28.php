<?php

/*

type: layout

name: Alert 3

position: 28

*/
?>


<div class="edit nodrop safe-mode" field="layout-skin-28-<?php print $params['id'] ?>" rel="module">
    <div class="callout alert alert-transparent noborder noradius nomargin">

        <div class="text-center allow-drop">
            <h3>Call now at <span><strong>+800-565-2390</strong></span> and get 15% discount!</h3>
            <p class="font-lato size-20">
                We truly care about our users and our product.
            </p>
            <br/>
            <module type="btn" template="bootstrap"/>
            <br/>
            <br/>
        </div>

    </div>
</div>