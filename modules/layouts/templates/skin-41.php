<?php

/*

type: layout

name: Three column blocks

position: 41

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-41-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-4 cloneable allow-drop" style="padding-bottom: 20px;">
                <div class="heading-title heading-border-bottom heading-color nodrop">
                    <h3>Simplicity</h3>
                </div>

                <p>
                    Fabulas definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii propriae philosophia cu mea. Utinam ipsum everti necessitatibus at fuisset splendide.
                </p>

                <a href="#" class="safe-element nodrop">
                    Read more <i class="glyphicon glyphicon-menu-right size-12 safe-element"></i>
                </a>
            </div>

            <div class="col-md-4 cloneable allow-drop" style="padding-bottom: 20px;">
                <div class="heading-title heading-border-bottom heading-color nodrop">
                    <h3>Simplicity</h3>
                </div>

                <p>
                    Fabulas definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii propriae philosophia cu mea. Utinam ipsum everti necessitatibus at fuisset splendide.
                </p>

                <a href="#" class="safe-element nodrop">
                    Read more <i class="glyphicon glyphicon-menu-right size-12 safe-element"></i>
                </a>
            </div>

            <div class="col-md-4 cloneable allow-drop" style="padding-bottom: 20px;">
                <div class="heading-title heading-border-bottom heading-color nodrop">
                    <h3>Simplicity</h3>
                </div>

                <p>
                    Fabulas definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii propriae philosophia cu mea. Utinam ipsum everti necessitatibus at fuisset splendide.
                </p>

                <a href="#" class="safe-element nodrop">
                    Read more <i class="glyphicon glyphicon-menu-right size-12 safe-element"></i>
                </a>
            </div>
        </div>

    </div>
</section>
