<?php

/*

type: layout

name: Products in Slider

position: 3

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-3-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <h2 class="owl-featured noborder"><strong>FEATURED</strong> PRODUCTS</h2>
        <module type="shop/products" template="skin-1"/>
    </div>
</section>