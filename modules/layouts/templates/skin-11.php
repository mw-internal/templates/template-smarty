<?php

/*

type: layout

name: Full Witdh Link

position: 11

*/
?>
<?php
/* Style */
$style = get_option('style', $params['id']);
if ($style === null OR $style === false OR $style == '') {
    $style = 'btn-default';
}

$url = get_option('url', $params['id']);
if ($url === null OR $url === false OR $url == '') {
    $url = '#';
}

$text = get_option('text', $params['id']);
if ($text === null OR $text === false OR $text == '') {
    $text = 'Did Smarty convinced you? <strong>Purchase now &raquo;</strong>';
}
?>
<div class="edit nodrop safe-mode" field="layout-skin-11-<?php print $params['id'] ?>" rel="module">
    <a href="<?php print $url; ?>" target="_blank" class="btn btn-xlg <?php print $style; ?> size-20 fullwidth nomargin noradius padding-40 noborder-left noborder-right noborder-bottom safe-element">
        <span class="font-lato size-30 safe-element"><?php print $text; ?></span>
    </a>
</div>