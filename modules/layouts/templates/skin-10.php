<?php

/*

type: layout

name: Layout 9

position: 10

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-10-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <module type="tabs" id="home-accordion" template="skin-2"/>
            </div>

            <div class="col-md-4 col-sm-4">
                <h4>Our Skills</h4>

                <module type="skills"/>
            </div>

            <div class="col-md-4 col-sm-4">
                <h4>Recent News</h4>
                <module type="posts" template="skin-5" id="recent-news-<?php print content_id(); ?>" limit="3"/>
            </div>
        </div>
    </div>
</section>