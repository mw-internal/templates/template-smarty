<?php

/*

type: layout

name: Image Cards

position: 42

*/
?>

<section class="edit nodrop safe-mode" field="layout-skin-42-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 cloneable" style="padding-bottom: 25px;">
                <figure class="margin-bottom-20 safe-element">
                    <img class="img-responsive safe-element" src="<?php print template_url('assets/images/'); ?>demo/mockups/800x553/17-min.jpg" alt="" />
                </figure>

                <h4 class="nomargin-bottom">WHY SMARTY?</h4>
                <small class="font-lato size-18 margin-bottom-30 block safe-element">Because we are the best</small>
                <p class="text-muted size-14">Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro.</p>

                <a href="#" class="safe-element">
                    Read more
                    <i class="glyphicon glyphicon-menu-right size-12 safe-element"></i>
                </a>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 cloneable" style="padding-bottom: 25px;">
                <figure class="margin-bottom-20 safe-element">
                    <img class="img-responsive safe-element" src="<?php print template_url('assets/images/'); ?>demo/mockups/800x553/18-min.jpg" alt="" />
                </figure>

                <h4 class="nomargin-bottom">SMARTY MISSION</h4>
                <small class="font-lato size-18 margin-bottom-30 block safe-element">To be best of the best</small>
                <p class="text-muted size-14">Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro.</p>

                <a href="#" class="safe-element">
                    Read more
                    <i class="glyphicon glyphicon-menu-right size-12 safe-element"></i>
                </a>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 cloneable" style="padding-bottom: 25px;">
                <figure class="margin-bottom-20 safe-element">
                    <img class="img-responsive safe-element" src="<?php print template_url('assets/images/'); ?>demo/mockups/800x553/3-min.jpg" alt="" />
                </figure>

                <h4 class="nomargin-bottom">OUR CONCEPT</h4>
                <small class="font-lato size-18 margin-bottom-30 block safe-element">It's very simple but powerful</small>
                <p class="text-muted size-14">Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro.</p>

                <a href="#" class="safe-element">
                    Read more
                    <i class="glyphicon glyphicon-menu-right size-12 safe-element"></i>
                </a>
            </div>
        </div>
    </div>
</section>
