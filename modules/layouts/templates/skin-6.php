<?php

/*

type: layout

name: Alert Color

position: 6

*/
?>

<div class="alert alert-transparent bordered-bottom nomargin edit nodrop safe-mode" field="layout-skin-6-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <h3>Call now at <span><strong>+800-565-2390</strong></span> and get 15% discount!</h3>
                <p class="font-lato weight-300 size-20 nomargin-bottom">We truly care about our users and our product.</p>
            </div>

            <div class="col-md-3 col-sm-12 text-right">
                <module type="btn" button_style="btn-primary" button_size="btn-lg"/>
            </div>
        </div>
    </div>
</div>