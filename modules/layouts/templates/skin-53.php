<?php

/*

type: layout

name: Three Videos in Row With Text

position: 53

*/
?>

<section class="edit nodrop" field="layout-skin-53-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-4 allow-drop">
                <module type="video"/>
                <h4><b>Vimeo</b> Video</h4>
                <p>Some video description</p>
            </div>
            <div class="col-md-4 allow-drop">
                <module type="video"/>
                <h4><b>Youtube</b> Video</h4>
                <p>Some video description</p>
            </div>
            <div class="col-md-4 allow-drop">
                <module type="video"/>
                <h4><b>Vimeo</b> Video</h4>
                <p>Some video description</p>
            </div>
        </div>
    </div>
</section>