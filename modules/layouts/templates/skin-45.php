<?php

/*

type: layout

name: Text box

position: 45

*/
?>

<section class="callout-dark heading-title heading-arrow-bottom edit nodrop safe-mode" field="layout-skin-45-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="text-center allow-drop">
            <h3 class="size-30">Smarty Multipurpose Responsive Template</h3>
            <p>We can't solve problems by using the same kind of thinking we used when we created them.</p>
        </div>
    </div>
</section>