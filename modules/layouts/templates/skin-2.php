<?php

/*

type: layout

name: Text with Icons

description: Text with Icons

position: 2

*/
?>

<section class="info-bar info-bar-clean nodrop safe-mode edit" field="layout-skin-2-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 cloneable">
                <i class="fa fa-globe safe-element"></i>
                <h3>FREE SHIPPING &amp; RETURN</h3>
                <p>Free shipping on all orders over $99.</p>
            </div>

            <div class="col-sm-3 cloneable">
                <i class="fa fa-usd safe-element"></i>
                <h3>MONEY BACK GUARANTEE</h3>
                <p>100% money back guarantee.</p>
            </div>

            <div class="col-sm-3 cloneable">
                <i class="fa fa-flag safe-element"></i>
                <h3>ONLINE SUPPORT 24/7</h3>
                <p>Lorem ipsum dolor sit amet.</p>
            </div>

            <div class="col-sm-3 cloneable">
                <i class="fa fa-heart safe-element"></i>
                <h3><span class="countTo">9845</span> HAPPY CUSTOMERS</h3>
                <p>Lorem ipsum dolor sit amet.</p>
            </div>
        </div>
    </div>
</section>