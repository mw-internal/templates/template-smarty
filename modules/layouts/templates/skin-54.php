<?php

/*

type: layout

name: Parallax with text

position: 54

*/
?>

<?php
/* Overlay */
$overlay = get_option('overlay', $params['id']);
if ($overlay === null OR $overlay === false) {
    $overlay = '6';
}
?>

<section class="parallax parallax-2 edit nodrop" style="background-image: url('<?php print template_url('assets/images/'); ?>demo/wall3-min.jpg');" field="layout-skin-54-<?php print $params['id'] ?>" rel="module">
    <div class="overlay dark-<?php print $overlay; ?>"></div>

    <div class="container">
        <div class="text-center allow-drop">
            <h3 class="nomargin">Parallax</h3>
            <p class="font-lato weight-300 lead nomargin-top">We can't solve problems by using the same kind of thinking we used when we created them.</p>
        </div>

        <div class="margin-top-20 text-center allow-drop">
            <module type="btn" />
        </div>
    </div>
</section>