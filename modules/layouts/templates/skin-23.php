<?php

/*

type: layout

name: Testimonials

position: 23

*/
?>


<section class="edit nodrop safe-mode" field="layout-skin-23-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <module type="testimonials" template="default"/>
    </div>
</section>