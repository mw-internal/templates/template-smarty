<?php

/*

type: layout

name: Title 2

position: 26

*/
?>


<section class="page-header page-header-lg parallax parallax-4 edit nodrop safe-mode" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')" field="layout-skin-26-<?php print $params['id'] ?>" rel="module">
    <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

    <div class="container">
        <h1>Title</h1>
        <span class="font-lato size-18 weight-300 safe-element">We believe in Simple &amp; Creative</span>

        <module type="breadcrumb" id="services-1-breadcrumbs-<?php print PAGE_ID; ?>"/>
    </div>
</section>