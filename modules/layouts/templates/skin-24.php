<?php

/*

type: layout

name: Posts

position: 24

*/
?>


<section class="edit nodrop safe-mode" field="layout-skin-24-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <module type="posts" template="skin-3"/>
    </div>
</section>