<?php

/*

type: layout

name: Contact form

position: 58

*/
?>


<section class="edit nodrop safe-mode" field="layout-skin-32-<?php print $params['id'] ?>" rel="module" style="background:url('<?php print template_url('assets/images/'); ?>demo/wall2.jpg')">
    <div class="display-table">
        <div class="display-table-cell vertical-align-middle">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-md-push-7 col-lg-push-8 col-sm-push-7">
                        <module type="contact_form" template="skin-2"/>
                    </div>

                    <div class="col-xs-12 col-md-7 col-sm-7 col-lg-8 col-lg-pull-4 col-md-pull-5 col-sm-pull-5 allow-drop">
                        <h2 class="size-20 text-center-xs">Why Smarty?</h2>

                        <p>Lorem ipsum dolor sit amet. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.Lorem ipsum dolor
                            sit amet. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>

                        <ul class="list-unstyled login-features nodrop">
                            <li>
                                <i class="glyphicon glyphicon-road"></i> <strong>Lorem ipsum</strong> dolor sit amet.
                            </li>
                            <li>
                                <i class="glyphicon glyphicon-cog"></i> <strong>Sed ut perspiciatis</strong> unde omnis iste.
                            </li>
                            <li>
                                <i class="glyphicon glyphicon-tint"></i> <strong>Et harum quidem</strong> rerum facilis est et expedita distinctio.
                            </li>
                            <li>
                                <i class="glyphicon glyphicon-screenshot"></i> <strong>Nam libero</strong> tempore, cum soluta nobis.
                            </li>
                            <li>
                                <i class="glyphicon glyphicon-fire"></i> <strong>Est eligendi</strong> voluptatem accusantium.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>