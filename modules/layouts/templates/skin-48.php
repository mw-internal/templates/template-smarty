<?php

/*

type: layout

name: Simple Text

position: 48

*/
?>


<div class="edit clean-container nodrop" field="layout-skin-48-<?php print $params['id'] ?>" rel="module">
    <div class="container allow-drop">
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam in minima iusto voluptatem aliquam odit odio. Aliquam voluptatibus beatae officiis?</p>
    </div>
</div>