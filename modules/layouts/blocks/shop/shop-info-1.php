<?php
$next = next_content();

$prev = prev_content();
?>

<script>mw.moduleCSS("<?php print modules_url(); ?>layouts/blocks/styles.css"); </script>

<!-- buttons -->
<!--<div class="pull-right">-->
<!--    <a class="btn btn-default add-wishlist" href="#" data-item-id="1" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart nopadding"></i></a>-->
<!--    <a class="btn btn-default add-compare" href="#" data-item-id="1" data-toggle="tooltip" title="Add To Compare"><i class="fa fa-bar-chart-o nopadding" data-toggle="tooltip"></i></a>-->
<!--</div>-->
<!-- /buttons -->

<!-- price -->
<div class="shop-item-price">
    <?php print currency_format(get_product_price()); ?>
</div>
<!-- /price -->

<hr/>

<div class="clearfix margin-bottom-30">
    <?php $content_data = content_data(CONTENT_ID);
    $in_stock = true;
    if (isset($content_data['qty']) and $content_data['qty'] != 'nolimit' and intval($content_data['qty']) == 0) {
        $in_stock = false;
    }
    ?>
    <?php if ($in_stock == true): ?>
        <span class="pull-right text-success"><i class="fa fa-check"></i> In Stock</span>
    <?php else: ?>
        <span class="pull-right text-danger"><i class="glyphicon glyphicon-remove"></i> Out of Stock</span>
    <?php endif; ?>
    <?php if (isset($content_data['sku'])): ?>
        <strong>SKU:</strong> <?php print $content_data['sku']; ?>
    <?php endif; ?>
</div>


<div class="edit" field="content_fulldescription" rel="post">
    <p>This text is set by default and is suitable for edit in real time. By default the drag and drop core feature will allow you to position it
        anywhere on the site. Get creative &amp; <strong>Make Web</strong>.</p>
</div>

<hr/>

<div class="clearfix form-inline nomargin">
    <module type="shop/cart_add"/>
</div>

<hr/>

<div class="pull-right">
    <module type="sharer" id="product-sharer">
</div>


<!-- rating -->
<module type="rating" content-id="<?php print CONTENT_ID; ?>"/>
<!-- /rating -->





