<?php

/*

type: layout

name: About page Skills

description: Skin 1

*/
?>
<div class="mw-module-skills-list">
    <div class="skills">
        <?php
        $file = get_option('file', $params['id']);
        if (!!$file) {
            $skills = json_decode($file, true);

            foreach ($skills as $skill) { ?>
                <?php
                $color = 'progress-bar-' . $skill['style'];
                $percent = 'progress-bar-' . $skill['style'];
                if (isset($skill['percent'])) {
                    $percent = $skill['percent'];
                } else {
                    $percent = 50;
                }
                ?>
                <div class="progress progress-lg"><!-- progress bar -->
                    <div class="progress-bar <?php print $color; ?> progress-bar-striped active text-left" role="progressbar" aria-valuenow="<?php print $percent; ?>" aria-valuemin="0" aria-valuemax="100"
                         style="width: <?php print $percent; ?>%; min-width: 2em;">
                        <span><?php print $skill['skill']; ?> <?php print $percent; ?>%</span>
                    </div>
                </div>
            <?php }
        } else {
            print lnotif('Click to insert skills');
        }
        ?>
    </div>
</div>