<?php

/*

type: layout

name: Add to cart default

description: Add to cart default

*/


?>
<?php

if (isset($params['content-id'])) {
    $product = get_content_by_id($params["content-id"]);
    $title = $product['title'];
} else {
    $title = _e("Product", true);
}


?>

<module type="custom_fields" data-content-id="<?php print intval($for_id); ?>" data-skip-type="price" id="cart_fields_<?php print $params['id'] ?>"/>
<?php if (is_array($data)): ?>
    <div class="price">
    <?php $i = 1;
    foreach ($data as $key => $v): ?>

        <?php if (is_string($key) and trim(strtolower($key)) == 'price'): ?>
            <?php _lang($key, "templates/smarty"); ?>
        <?php else: ?>
            <?php print $key; ?>
        <?php endif; ?>: <?php print currency_format($v); ?>
        <?php if (!isset($in_stock) or $in_stock == false) : ?>
            <button class="btn btn-primary pull-left product-add-cart noradius" type="button" disabled="disabled"
                    onclick="Alert('<?php print addslashes(_e("This item is out of stock and cannot be ordered", true)); ?>');">
                <?php _lang("Out of stock", "templates/smarty"); ?>
            </button>
        <?php else: ?>
            <button class="btn btn-primary pull-left product-add-cart noradius" type="button"
                    onclick="mw.cart.add('.mw-add-to-cart-<?php print $params['id'] ?>','<?php print $v ?>', '<?php print $title; ?>');">
                <?php _lang($button_text, "templates/smarty" !== false ? $button_text : "Add to cart", "templates/smarty"); ?>
            </button>
            <?php $i++; endif; ?>
        </div>
        <?php if ($i > 1) : ?>
            <br/>
        <?php endif; ?>
        <?php $i++; endforeach; ?>
<?php endif; ?>
