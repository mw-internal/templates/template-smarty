<?php

/*

type: layout

name: Checkout 1

description: Checkout 1 cart template

*/

?>

<div>
    <?php if ($requires_registration and is_logged() == false): ?>
        <module type="users/register"/>
    <?php else: ?>
        <div class="clear"></div>
        <?php if ($payment_success == false): ?>
            <form class="mw-checkout-form" id="checkout_form_<?php print $params['id'] ?>" method="post" action="<?php print api_link('checkout') ?>">
                <div class="alert hide"></div>


                <?php $cart_show_enanbled = get_option('data-show-cart', $params['id']); ?>
                <div <?php if ($step != 1): ?>style="display: none;"<?php endif; ?>>
                    <?php if ($cart_show_enanbled != 'n'): ?>
                        <div class="mw-cart-data-holder">
                            <div class="row ">
                                <div class="col-lg-9 col-sm-8">
                                    <module type="shop/cart" template="big-1" id="cart_checkout_<?php print $params['id'] ?>" data-checkout-link-enabled="n"/>
                                </div>
                                <div class="col-lg-3 col-sm-4">

                                    <div class="toggle-transparent toggle-bordered-full clearfix">

                                        <div class="toggle nomargin-top">
                                            <label>Voucher</label>

                                            <div class="toggle-content">
                                                <div class="nomargin">
                                                    <module type="shop/coupons"/>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="toggle">
                                            <label>Shipping tax calculator</label>
                                            <div class="toggle-content">
                                                <p>To get a shipping estimate, please enter your destination.</p>

                                                <div class="nomargin">
                                                    <div>
                                                        <label><?php _lang("Shipping to", "templates/smarty"); ?>:</label>
                                                        <module type="shop/shipping" view="select"/>
                                                    </div>
                                                    <br/>
                                                    <label><?php _lang("Shipping price", "templates/smarty"); ?>:</label>
                                                    <module type="shop/shipping" view="cost"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="toggle-transparent toggle-bordered-full clearfix">
                                        <div class="toggle active">
                                            <div class="toggle-content">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h4><?php _lang('Shipping information', "templates/smarty"); ?></h4>
                                                        <hr/>
                                                    </div>
                                                </div>

                                                <?php if (function_exists('cart_get_tax') and get_option('enable_taxes', 'shop') == 1) : ?>
                                                    <span class="clearfix">
                                                    <span class="pull-right"><?php print currency_format(cart_get_tax()); ?></span>
                                                    <span class="pull-left"><?php _lang("TAX", "templates/smarty"); ?>:</span>
                                                </span>
                                                    <hr/>
                                                <?php endif; ?>

                                                <span class="clearfix">
                                                <span class="pull-right size-20"><?php print currency_format(cart_sum()); ?></span>
                                                <strong class="pull-left"><?php _lang("TOTAL", "templates/smarty"); ?>:</strong>
                                            </span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endif; ?>

                    <a href="?step=2" class="btn btn-primary pull-right">
                        <?php _lang("PROCEED TO SHIPMENT", "templates/smarty"); ?>
                    </a>
                </div>


                <div class="mw-cart-data-holder">
                    <div <?php if ($step != 2): ?>style="display: none;"<?php endif; ?>>


                        <div class="col-lg-8 col-sm-8">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="edit nodrop" field="checkout_personal_inforomation_title" rel="global"
                                        rel_id="<?php print $params['id'] ?>"><?php _lang("Personal Information", "templates/smarty"); ?></h4>
                                    <hr/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <label><?php _lang("First Name", "templates/smarty"); ?></label>
                                    <input name="first_name" class="form-control" type="text" value="<?php if (isset($user['first_name'])) {
                                        print $user['first_name'];
                                    } ?>"/>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <label for="last_name"><?php _lang("Last Name", "templates/smarty"); ?></label>
                                    <input name="last_name" class="form-control" type="text" value="<?php if (isset($user['last_name'])) {
                                        print $user['last_name'];
                                    } ?>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <label for="email"><?php _lang("Email", "templates/smarty"); ?></label>
                                    <input name="email" class="form-control" type="text" value="<?php if (isset($user['email'])) {
                                        print $user['email'];
                                    } ?>"/>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <label for="last_name"><?php _lang("Phone", "templates/smarty"); ?></label>
                                    <input name="phone" class="form-control" type="text" value="<?php if (isset($user['phone'])) {
                                        print $user['phone'];
                                    } ?>"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <?php if ($cart_show_shipping != 'n'): ?>
                                <div class="mw-shipping-and-payment">
                                    <module type="shop/shipping"/>
                                </div>
                            <?php endif; ?>

                            <module type="shop/checkout/terms" />

                        </div>

                        <div class="col-lg-4 col-sm-4">
                            <?php if ($cart_show_payments != 'n'): ?>
                                <div class="mw-shipping-and-payment">
                                    <module type="shop/payments"/>
                                </div>
                            <?php endif; ?>
                        </div>


                        <div class="col-xs-12">
                            <a href="?step=1" class="btn btn-default pull-left">
                                <?php _lang("Go back to cart", "templates/smarty"); ?>
                            </a>

                            <button class="btn btn-primary pull-right" onclick="mw.cart.checkout('#checkout_form_<?php print $params['id'] ?>');" type="button"
                                    id="complete_order_button" <?php if ($tems): ?> disabled="disabled"   <?php endif; ?>>
                                <?php _lang("Complete order", "templates/smarty"); ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mw-checkout-responce"></div>
            </form>

        <?php else: ?>
            <h2>
                <?php _lang("Your payment was successfull.", "templates/smarty"); ?>
            </h2>
        <?php endif; ?>
    <?php endif; ?>
</div>