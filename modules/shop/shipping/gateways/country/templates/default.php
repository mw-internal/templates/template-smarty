<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="<?php print $config['module_class'] ?>">
    <div id="<?php print $rand; ?>">
        <?php $selected_country = mw()->user_manager->session_get('shipping_country'); ?>


        <div class="row">
            <div class="col-xs-12 col-md-6">
                <label for="country"><?php _lang("Country", "templates/smarty"); ?></label>

                <select name="country" class="form-control">
                    <option value=""><?php _lang("Country", "templates/smarty"); ?></option>
                    <?php foreach ($data as $item): ?>
                        <option value="<?php print $item['shipping_country'] ?>" <?php if (isset($selected_country) and $selected_country == $item['shipping_country']): ?> selected="selected" <?php endif; ?>><?php print $item['shipping_country'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-xs-12 col-md-6">
                <label for="city"><?php _lang("Town / City", "templates/smarty"); ?></label>
                <input name="Address[city]" class="form-control" type="text" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <label for="zip"><?php _lang("ZIP / Postal Code", "templates/smarty"); ?></label>
                <input name="Address[zip]" class="form-control" type="text" value=""/>
            </div>

            <div class="col-xs-12 col-md-6">
                <label for="state"><?php _lang("State / Province", "templates/smarty"); ?></label>
                <input name="Address[state]" class="form-control" type="text" value=""/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <label for="address"><?php _lang("Address", "templates/smarty"); ?></label>
                <input name="Address[address]" class="form-control" type="text" value="" placeholder="Street address, Floor, Apartment, etc..."/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <label for="note"><?php _lang("Note", "templates/smarty"); ?></label>
                <input name="other_info" class="form-control" type="text" value="" placeholder="Additional Information ( Special notes for delivery - Optional )"/>
            </div>
        </div>

    </div>
</div>
