<?php

/*

type: layout

name: Product List 2 - Full width items

description: Product List 2 - Full width items

*/
?>
<?php


$tn = $tn_size;
if (!isset($tn[0]) or ($tn[0]) == 150) {
    $tn[0] = 350;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}


?>
<?php if (!empty($data)): ?>
    <ul class="shop-item-list row list-inline nomargin">
        <?php foreach ($data as $item): ?>
            <li class="col-lg-12">
                <div class="shop-item clearfix" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                    <div class="thumbnail">
                        <?php if ($show_fields == false or in_array('thumbnail', $show_fields)): ?>
                            <a class="shop-item-image" href="<?php print $item['link'] ?>">
                                <img class="img-responsive" src="<?php print thumbnail($item['image'], 350, 525, true); ?>" alt="<?php print $item['title'] ?>" itemprop="image"/>
                            </a>
                        <?php endif; ?>
                    </div>

                    <div class="shop-item-summary">
                        <?php if ($show_fields == false or in_array('title', $show_fields)): ?>
                            <h2 itemprop="name"><?php print $item['title'] ?></h2>
                        <?php endif; ?>

                        <module type="rating" template="skin-3" content_id="<?php print $item['id']; ?>"/>

                        <?php if ($show_fields == false or in_array('description', $show_fields)): ?>
                            <p><?php print $item['description'] ?></p>
                        <?php endif; ?>

                        <?php if ($show_fields == false or in_array('price', $show_fields)): ?>
                            <?php if (isset($item['prices']) and is_array($item['prices'])) { ?>
                                <?php
                                $vals2 = array_values($item['prices']);
                                $val1 = array_shift($vals2); ?>
                                <div class="shop-item-price"><?php print currency_format($val1); ?></div>
                            <?php } else { ?>
                            <?php } ?>
                        <?php endif; ?>

                        <div class="shop-item-buttons">
                            <?php if ($show_fields == false or ($show_fields != false and in_array('add_to_cart', $show_fields))): ?>
                                <a class="btn btn-default" href="javascript:;" onclick="mw.cart.add_and_checkout('<?php print $item['id'] ?>');" data-toggle="tooltip" title="Add To Cart">
                                    <i class="fa fa-cart-plus"></i> <?php $add_cart_text ? print $add_cart_text : 'Add to cart' ?>
                                </a>
                            <?php endif; ?>
                            <?php if ($show_fields == false or ($show_fields != false and in_array('read_more', $show_fields))): ?>
                                <a class="btn btn-default" itemprop="url" href="<?php print $item['link'] ?>" data-toggle="tooltip" title="Read More">
                                    <i class="fa fa-search"></i> <?php $read_more_text ? print $read_more_text : 'Read more' ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php if (is_array($item['prices'])): ?>
                        <?php foreach ($item['prices'] as $k => $v): ?>
                            <input type="hidden" name="price" value="<?php print $v ?>"/>
                            <input type="hidden" name="content_id" value="<?php print $item['id'] ?>"/>
                            <?php break; endforeach; ?>
                    <?php endif; ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <hr/>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
