<?php

/*

type: layout

name: Product List 1 - Carousel - 4 Items

description: Product List 1 - Carousel - 4 Items

*/
?>
<?php


$tn = $tn_size;
if (!isset($tn[0]) or ($tn[0]) == 150) {
    $tn[0] = 350;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}

?>
<?php if (!empty($data)): ?>
    <script>
        $(document).ready(function () {
            var _container = jQuery(".owl-carousel-product-list-1", '#<?php print $params['id'] ?>');

            if (_container.length > 0) {

                _container.each(function () {

                    var slider = jQuery(this);
                    var options = slider.attr('data-plugin-options');

                    // Progress Bar
                    var $opt = eval('(' + options + ')');  // convert text to json

                    if ($opt.progressBar == 'true') {
                        var afterInit = progressBar;
                    } else {
                        var afterInit = false;
                    }

                    var defaults = {
                        items: 5,
                        itemsCustom: false,
                        itemsDesktop: [1199, 4],
                        itemsDesktopSmall: [980, 3],
                        itemsTablet: [768, 2],
                        itemsTabletSmall: false,
                        itemsMobile: [479, 1],
                        singleItem: true,
                        itemsScaleUp: false,

                        slideSpeed: 200,
                        paginationSpeed: 800,
                        rewindSpeed: 1000,

                        autoPlay: false,
                        stopOnHover: false,

                        navigation: false,
                        navigationText: [
                            '<i class="fa fa-angle-left"></i>',
                            '<i class="fa fa-angle-right"></i>'
                        ],
                        rewindNav: true,
                        scrollPerPage: false,

                        pagination: true,
                        paginationNumbers: false,

                        responsive: true,
                        responsiveRefreshRate: 200,
                        responsiveBaseWidth: window,

                        baseClass: "owl-carousel",
                        theme: "owl-theme",

                        lazyLoad: false,
                        lazyFollow: true,
                        lazyEffect: "fade",

                        autoHeight: false,

                        jsonPath: false,
                        jsonSuccess: false,

                        dragBeforeAnimFinish: true,
                        mouseDrag: true,
                        touchDrag: true,

                        transitionStyle: false,

                        addClassActive: false,

                        beforeUpdate: false,
                        afterUpdate: false,
                        beforeInit: false,
                        afterInit: afterInit,
                        beforeMove: false,
                        afterMove: (afterInit == false) ? false : moved,
                        afterAction: false,
                        startDragging: false,
                        afterLazyLoad: false
                    }

                    var config = jQuery.extend({}, defaults, options, slider.data("plugin-options"));
                    slider.owlCarousel(config).addClass("owl-carousel-init");


                    // Progress Bar
                    var elem = jQuery(this);

                    //Init progressBar where elem is $("#owl-demo")
                    function progressBar(elem) {
                        $elem = elem;
                        //build progress bar elements
                        buildProgressBar();
                        //start counting
                        start();
                    }

                    //create div#progressBar and div#bar then prepend to $("#owl-demo")
                    function buildProgressBar() {
                        $progressBar = jQuery("<div>", {
                            id: "progressBar"
                        });
                        $bar = jQuery("<div>", {
                            id: "bar"
                        });
                        $progressBar.append($bar).prependTo($elem);
                    }

                    function start() {
                        //reset timer
                        percentTime = 0;
                        isPause = false;
                        //run interval every 0.01 second
                        tick = setInterval(interval, 10);
                    };


                    var time = 7; // time in seconds
                    function interval() {
                        if (isPause === false) {
                            percentTime += 1 / time;
                            $bar.css({
                                width: percentTime + "%"
                            });
                            //if percentTime is equal or greater than 100
                            if (percentTime >= 100) {
                                //slide to next item
                                $elem.trigger('owl.next')
                            }
                        }
                    }

                    //pause while dragging
                    function pauseOnDragging() {
                        isPause = true;
                    }

                    //moved callback
                    function moved() {
                        //clear interval
                        clearTimeout(tick);
                        //start again
                        start();
                    }
                });
            }
        });
    </script>

    <div class="owl-carousel-product-list-1 featured nomargin owl-padding-10"
         data-plugin-options='{"singleItem": false, "items": "4", "stopOnHover":false, "autoPlay":4000, "autoHeight": false, "navigation": true, "pagination": false}'>
        <?php foreach ($data as $item): ?>
            <div class="shop-item nomargin" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">


                <?php if ($show_fields == false or in_array('thumbnail', $show_fields)): ?>
                    <div class="thumbnail">
                        <a class="shop-item-image" href="<?php print $item['link'] ?>">
                            <?php
                            $item_pictures = get_pictures($item['id']);
                            if ($item_pictures) {
                                ?>
                                <?php foreach ($item_pictures as $key => $picture): ?>
                                    <?php if ($key == 0): ?>
                                        <img class="img-responsive" src="<?php print thumbnail($picture['filename'], 350, 525, true); ?>" alt="shop first image" itemprop="image"/>
                                    <?php endif; ?>
                                    <?php if ($key == 1): ?>
                                        <img class="img-responsive" src="<?php print thumbnail($picture['filename'], 350, 525, true); ?>" alt="shop hover image" itemprop="image"/>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php } ?>
                        </a>
                    </div>
                <?php endif; ?>


                <div class="shop-item-summary text-center">
                    <?php if ($show_fields == false or in_array('title', $show_fields)): ?>
                        <h2 itemprop="name"><?php print $item['title'] ?></h2>
                    <?php endif; ?>

                    <module type="rating" template="skin-2" content_id="<?php print $item['id']; ?>"/>

                    <?php if ($show_fields == false or in_array('price', $show_fields)): ?>
                        <?php if (isset($item['prices']) and is_array($item['prices'])) { ?>
                            <?php
                            $vals2 = array_values($item['prices']);
                            $val1 = array_shift($vals2); ?>
                            <div class="shop-item-price"><?php print currency_format($val1); ?></div>
                        <?php } else { ?>
                        <?php } ?>
                    <?php endif; ?>
                </div>

                <?php if ($show_fields == false or ($show_fields != false and in_array('add_to_cart', $show_fields))): ?>
                    <div class="shop-item-buttons text-center">
                        <a class="btn btn-default" href="javascript:;" onclick="mw.cart.add_and_checkout('<?php print $item['id'] ?>');"><i
                                    class="fa fa-cart-plus"></i> <?php $add_cart_text ? print $add_cart_text : 'Add to cart' ?></a>
                    </div>
                <?php endif; ?>

                <?php if ($show_fields == false or ($show_fields != false and in_array('read_more', $show_fields))): ?>
                    <div class="shop-item-buttons text-center">
                        <a class="btn btn-default" itemprop="url" href="<?php print $item['link'] ?>"><i
                                    class="fa fa-search"></i> <?php $read_more_text ? print $read_more_text : 'Read more' ?></a>
                    </div>
                <?php endif; ?>

                <?php if (is_array($item['prices'])): ?>
                    <?php foreach ($item['prices'] as $k => $v): ?>
                        <input type="hidden" name="price" value="<?php print $v ?>"/>
                        <input type="hidden" name="content_id" value="<?php print $item['id'] ?>"/>
                        <?php break; endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
