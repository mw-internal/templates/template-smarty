<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="mw-shipping-and-payments">
    <?php if (count($payment_options) > 0): ?>
        <div class="heading-title">
            <h4 class="edit nodrop" field="checkout_payment_information_title" rel="global" rel_id="<?php print $params['id'] ?>"><?php _lang("Payment method", "templates/smarty"); ?></h4>
        </div>

        <div class="">
            <fieldset class="margin-top-60">
                <div class="toggle-transparent toggle-bordered-full clearfix">
                    <div class="toggle active">
                        <div class="toggle-content">
                            <div class="row nomargin-bottom">
                                <div name="payment_gw" class="mw-payment-gateway-<?php print $params['id']; ?>">
                                    <?php $count = 0;
                                    foreach ($payment_options as $payment_option) : $count++; ?>
                                        <div class="col-lg-12 nomargin clearfix">
                                            <label for="option-<?php print $count; ?>" class="radio pull-left nomargin-top">
                                                <input type="radio" id="option-<?php print $count; ?>" <?php if ($count == 1): ?> checked="checked" <?php endif; ?>
                                                       value="<?php print  $payment_option['gw_file']; ?>"
                                                       name="payment_gw"/>
                                                <i></i> <span class="weight-300"><?php print  _e($payment_option['name']); ?></span>
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="toggle-transparent toggle-bordered-full clearfix">
                    <div class="toggle active">
                        <div class="toggle-content">
                            <div id="mw-payment-gateway-selected-<?php print $params['id']; ?>">
                                <?php if (isset($payment_options[0])): ?>
                                    <module type="<?php print $payment_options[0]['gw_file'] ?>"/>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="toggle-transparent toggle-bordered-full clearfix">
                <div class="toggle active">
                    <div class="toggle-content">
                        <?php $print_total = cart_total(); ?>
                        <span class="clearfix">
                            <span class="pull-right"><?php print currency_format($print_total); ?></span>
                            <strong class="pull-left"><?php _lang("Total", "templates/smarty"); ?>:</strong>
                        </span>
                        <span class="clearfix">
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>