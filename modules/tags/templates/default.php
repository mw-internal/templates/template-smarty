<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div>
    <?php foreach ($content_tags as $item): ?>
        <a href="<?php print $tags_url_base ?>/tags:<?php print url_title($item) ?>" class="tag"><span class="txt"><?php print $item ?></span></a>
    <?php endforeach; ?>
</div>