<?php

/*

type: layout

name: Clients

description: Skin 3

*/
?>

<?php if (!empty($data)): ?>
    <?php foreach ($data as $item): ?>
        <div class="row" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
            <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                <div class="col-md-2"><!-- company logo -->
                    <img src="<?php print thumbnail($item['image'], 600); ?>" class="img-responsive" alt="<?php print $item['title'] ?>">
                </div>
            <?php endif; ?>

            <div class="col-md-10"><!-- company detail -->
                <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                    <h4 class="margin-bottom-10"><?php print $item['title'] ?></h4>
                <?php endif; ?>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                    <p itemprop="description"><?php print $item['description'] ?></p>
                <?php endif; ?>
            </div>
        </div>

        <hr/>
    <?php endforeach; ?>
<?php endif; ?>

<div class="clearfix"></div>

<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
