<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="row mix-grid margin-top-80">
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item): ?>
            <div class="col-md-4 col-sm-4 mix design">
                <div class="item-box" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                        <figure>
                            <span class="item-hover">
                                <span class="overlay dark-5"></span>
                                <span class="inner">
                                    <!-- details -->
                                    <a class="ico-rounded" href="<?php print $item['link'] ?>" itemprop="url">
                                        <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                    </a>

                                </span>
                            </span>

                            <img class="img-responsive" src="<?php print thumbnail($item['image'], 600); ?>" width="600" height="399" alt="">
                        </figure>
                    <?php endif; ?>

                    <div class="item-box-desc">
                        <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                            <h3><?php print $item['title'] ?></h3>
                            <ul class="list-inline categories nomargin">
                                <?php
                                $categories = content_categories($item['id']);
                                if(!empty($categories)):
                                foreach ($categories as $category):
                                    ?>
                                    <li>
                                        <a class="category" href="<?php print category_link($category['id']); ?>">
                                            <span class="font-lato"><?php print $category['title']; ?></span>
                                        </a>
                                    </li>
                                <?php endforeach; endif; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

</div>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
