<?php

/*

type: layout

name: Home 3 - Posts

description: Skin 4

*/
?>

<div>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item): ?>
            <div class="blog-post-item" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                    <?php
                    $post_pictures = get_pictures($item['id']);
                    $cntPics = count($post_pictures);
                    if ($cntPics == 1) { ?>
                        <figure class="margin-bottom-20 text-center">
                            <img class="img-responsive" src="<?php print thumbnail($item['image'], 1200, 500, true); ?>" alt="">
                        </figure>
                    <?php } elseif ($cntPics > 1) { ?>
                        <script>
                            $(document).ready(function () {
                                var _container = jQuery(".owl-carousel-product-list-product-<?php print $item['id']; ?>", '#<?php print $params['id'] ?>');

                                if (_container.length > 0) {

                                    _container.each(function () {

                                        var slider = jQuery(this);
                                        var options = slider.attr('data-plugin-options');

                                        // Progress Bar
                                        var $opt = eval('(' + options + ')');  // convert text to json

                                        if ($opt.progressBar == 'true') {
                                            var afterInit = progressBar;
                                        } else {
                                            var afterInit = false;
                                        }

                                        var defaults = {
                                            items: 5,
                                            itemsCustom: false,
                                            itemsDesktop: [1199, 4],
                                            itemsDesktopSmall: [980, 3],
                                            itemsTablet: [768, 2],
                                            itemsTabletSmall: false,
                                            itemsMobile: [479, 1],
                                            singleItem: true,
                                            itemsScaleUp: false,

                                            slideSpeed: 200,
                                            paginationSpeed: 800,
                                            rewindSpeed: 1000,

                                            autoPlay: false,
                                            stopOnHover: false,

                                            navigation: false,
                                            navigationText: [
                                                '<i class="fa fa-angle-left"></i>',
                                                '<i class="fa fa-angle-right"></i>'
                                            ],
                                            rewindNav: true,
                                            scrollPerPage: false,

                                            pagination: true,
                                            paginationNumbers: false,

                                            responsive: true,
                                            responsiveRefreshRate: 200,
                                            responsiveBaseWidth: window,

                                            baseClass: "owl-carousel",
                                            theme: "owl-theme",

                                            lazyLoad: false,
                                            lazyFollow: true,
                                            lazyEffect: "fade",

                                            autoHeight: false,

                                            jsonPath: false,
                                            jsonSuccess: false,

                                            dragBeforeAnimFinish: true,
                                            mouseDrag: true,
                                            touchDrag: true,

                                            transitionStyle: false,

                                            addClassActive: false,

                                            beforeUpdate: false,
                                            afterUpdate: false,
                                            beforeInit: false,
                                            afterInit: afterInit,
                                            beforeMove: false,
                                            afterMove: (afterInit == false) ? false : moved,
                                            afterAction: false,
                                            startDragging: false,
                                            afterLazyLoad: false
                                        }

                                        var config = jQuery.extend({}, defaults, options, slider.data("plugin-options"));
                                        slider.owlCarousel(config).addClass("owl-carousel-init");


                                        // Progress Bar
                                        var elem = jQuery(this);

                                        //Init progressBar where elem is $("#owl-demo")
                                        function progressBar(elem) {
                                            $elem = elem;
                                            //build progress bar elements
                                            buildProgressBar();
                                            //start counting
                                            start();
                                        }

                                        //create div#progressBar and div#bar then prepend to $("#owl-demo")
                                        function buildProgressBar() {
                                            $progressBar = jQuery("<div>", {
                                                id: "progressBar"
                                            });
                                            $bar = jQuery("<div>", {
                                                id: "bar"
                                            });
                                            $progressBar.append($bar).prependTo($elem);
                                        }

                                        function start() {
                                            //reset timer
                                            percentTime = 0;
                                            isPause = false;
                                            //run interval every 0.01 second
                                            tick = setInterval(interval, 10);
                                        };


                                        var time = 7; // time in seconds
                                        function interval() {
                                            if (isPause === false) {
                                                percentTime += 1 / time;
                                                $bar.css({
                                                    width: percentTime + "%"
                                                });
                                                //if percentTime is equal or greater than 100
                                                if (percentTime >= 100) {
                                                    //slide to next item
                                                    $elem.trigger('owl.next')
                                                }
                                            }
                                        }

                                        //pause while dragging
                                        function pauseOnDragging() {
                                            isPause = true;
                                        }

                                        //moved callback
                                        function moved() {
                                            //clear interval
                                            clearTimeout(tick);
                                            //start again
                                            start();
                                        }
                                    });
                                }
                            });
                        </script>
                        <div class="owl-carousel-product-list-product-<?php print $item['id']; ?> buttons-autohide controlls-over"
                             data-plugin-options='{"items": 1, "autoPlay": 3000, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
                            <?php foreach ($post_pictures as $key => $picture): ?>
                                <?php if ($key < 3): ?>
                                    <div>
                                        <img class="img-responsive" src="<?php print thumbnail($picture['filename'], 1200, 500, true); ?>" alt="">
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php } ?>
                <?php endif; ?>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                    <h2><a href="<?php print $item['link'] ?>" itemprop="url"><?php print $item['title'] ?></a></h2>
                <?php endif; ?>

                <ul class="blog-post-info list-inline">
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                        <li>
                            <a href="<?php print $item['link'] ?>">
                                <i class="fa fa-clock-o"></i>
                                <span class="font-lato" itemprop="dateCreated"><?php print date('M d, Y', strtotime($item['created_at'])); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (function_exists('get_comments')) { ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-comment-o"></i>
                                <span class="font-lato"><?php print get_comments('count=1&content_id=' . $item['id']) ?> Comments</span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                    <p itemprop="description"><?php print $item['description'] ?></p>
                <?php endif; ?>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('read_more', $show_fields)): ?>
                    <a href="<?php print $item['link'] ?>" class="btn btn-reveal btn-default">
                        <i class="fa fa-plus"></i> <span><?php $read_more_text ? print $read_more_text : _e("Continue Reading"); ?></span>
                    </a>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
