<?php

/*

type: layout

name: Footer Recent Posts

description: Skin 6

*/
?>

<ul class="list-unstyled footer-list half-paddings">
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item): ?>
            <li>
                <a class="block" href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a>
                <small><?php print date('M d, Y', strtotime($item['created_at'])); ?></small>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
