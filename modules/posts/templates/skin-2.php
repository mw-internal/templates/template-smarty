<?php

/*

type: layout

name: Portfolio

description: Skin 2

*/
?>

<div id="portfolio" class="portfolio-nogutter">
    <div class="row mix-grid">
        <?php if (!empty($data)): ?>
            <?php foreach ($data as $item): ?>
                <?php
                $categories_filter = '';
                $categories_list = '';
                if (content_categories($item['id'])) {
                    foreach (content_categories($item['id']) as $category) {
                        $categories_filter .= ' filter-' . $category['id'];
                        $categories_list .= '<li><a href="' . category_link($category['id']) . '">' . $category['title'] . '</a></li> ';
                    }
                }
                ?>

                <div class="col-md-3 col-sm-3 mix <?php print $categories_filter; ?>">
                    <div class="item-box" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                        <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                            <figure>
                            <span class="item-hover">
                                <span class="overlay dark-5"></span>
                                <span class="inner">
                                    <!-- details -->
                                    <a class="ico-rounded" href="<?php print $item['link'] ?>" itemprop="url">
                                        <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                    </a>

                                </span>
                            </span>

                                <img class="img-responsive" src="<?php print thumbnail($item['image'], 600); ?>" width="600" height="399" alt="">
                            </figure>
                        <?php endif; ?>

                        <div class="item-box-desc">
                            <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                                <h3><?php print $item['title'] ?></h3>
                            <?php endif; ?>

                            <?php if ($categories_list != ''): ?>
                                <ul class="list-inline categories nomargin">
                                    <?php print $categories_list; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>

    </div>
</div>

<section class="page-header page-header-xs">
    <div class="container text-center">
        <?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
            <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
        <?php endif; ?>
    </div>
</section>
