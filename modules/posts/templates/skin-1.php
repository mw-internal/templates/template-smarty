<?php

/*

type: layout

name: Blog Timeline

description: Skin 1

*/
?>

<div class="timeline timeline-inverse">
    <div class="timeline-hline"></div>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item): ?>
            <div class="blog-post-item" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">

                <div class="timeline-entry"><!-- .rounded = entry -->
                    <?php print date('d', strtotime($item['created_at'])); ?>
                    <span><?php print date('M', strtotime($item['created_at'])); ?></span>
                    <div class="timeline-vline"></div>
                </div>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                    <a href="<?php print $item['link'] ?>" itemprop="url">
                        <img class="img-responsive" src="<?php print thumbnail($item['image'], 600); ?>" width="600" height="399" alt="">
                    </a>
                    <br/><br/>
                <?php endif; ?>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                    <h2><a href="<?php print $item['link'] ?>" itemprop="url"><?php print $item['title'] ?></a></h2>
                <?php endif; ?>

                <ul class="blog-post-info list-inline">
                    <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                        <li>
                            <a href="<?php print $item['link'] ?>">
                                <i class="fa fa-clock-o"></i>
                                <span class="font-lato" itemprop="dateCreated"><?php print date('M d, Y', strtotime($item['created_at'])); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (function_exists('get_comments')) { ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-comment-o"></i>
                                <span class="font-lato"><?php print get_comments('count=1&content_id=' . $item['id']) ?> Comments</span>
                            </a>
                        </li>
                    <?php } ?>

                    <li>
                        <i class="fa fa-folder-open-o"></i>

                        <?php
                        $categories = content_categories($item['id']);
                        if(!empty($categories)):
                        foreach ($categories as $category):
                            ?>
                            <a class="category" href="<?php print category_link($category['id']); ?>">
                                <span class="font-lato"><?php print $category['title']; ?></span>
                            </a>
                        <?php endforeach; endif; ?>
                    </li>

                    <?php if ($item['created_by']): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span class="font-lato"><?php print $item['created_by']; ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                    <p itemprop="description"><?php print $item['description'] ?></p>
                <?php endif; ?>

                <?php if (!isset($show_fields) or $show_fields == false or in_array('read_more', $show_fields)): ?>
                    <a href="<?php print $item['link'] ?>" class="btn btn-reveal btn-default">
                        <i class="fa fa-plus"></i> <span><?php $read_more_text ? print $read_more_text : _e("Continue Reading"); ?></span>
                    </a>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

</div>
<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
