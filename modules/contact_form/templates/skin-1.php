<?php

/*

type: layout

name: FAQ sidebar - Contact form

description: Skin 1

*/

?>

<div class="alert alert-success margin-bottom-30" id="msg<?php print $form_id; ?>" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Thank You!</strong> Your message successfully sent!
</div>

<form class="mw_form form sky-form clearfix" data-form-id="<?php print $form_id ?>" name="<?php print $form_id ?>" method="post">
    <?php print csrf_form() ?>
    <input type="hidden" name="for" value="contact_form"/>
    <input type="hidden" name="for_id" value="<?php print $params['id']; ?>"/>

    <label class="input">
        <i class="ico-prepend fa fa-user"></i>
        <input type="text" placeholder="<?php _lang('Name', "templates/smarty"); ?>" name="first_name" required>
    </label>

    <label class="input">
        <i class="ico-prepend fa fa-envelope"></i>
        <input type="email" placeholder="<?php _lang('Email', "templates/smarty"); ?>" name="email" required>
    </label>

    <module type="custom_fields" data-id="<?php print $params['id'] ?>" data-for="module" template="skin-1" default-fields="first_name,last_name,email,phone" input_class="form-control"/>


    <label class="textarea">
        <i class="ico-prepend fa fa-comment"></i>
        <textarea rows="3" placeholder="<?php _lang('Type your question...', "templates/smarty"); ?>" name="message" required></textarea>
    </label>

    <div class="row">
        <?php if (get_option('disable_captcha', $params['id']) != 'y'): ?>
            <module type="captcha"/>
        <?php endif; ?>
    </div>

    <button class="btn btn-primary btn-sm pull-right"><?php _lang('SUBMIT YOUR QUESTION', "templates/smarty"); ?></button>
</form>
