<?php

/*

type: layout

name: Default

description: Default contact form

*/

?>

<div class="alert alert-success margin-bottom-30" id="msg<?php print $form_id; ?>" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Thank You!</strong> Your message successfully sent!
</div>

<form class="mw_form form" data-form-id="<?php print $form_id ?>" name="<?php print $form_id ?>" method="post">
    <?php print csrf_form() ?>
    <input type="hidden" name="for" value="contact_form"/>
    <input type="hidden" name="for_id" value="<?php print $params['id']; ?>"/>


    <fieldset>
        <module type="custom_fields" data-id="<?php print $params['id'] ?>" data-for="module" template="skin-1" default-fields="first_name,last_name,email,phone" input_class="form-control"/>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <input type="text" placeholder="<?php _lang('Country', "templates/smarty"); ?>" name="country" class="form-control">
            </div>
            <div class="col-xs-12 col-sm-6">
                <input type="text" placeholder="<?php _lang('ZIP / Post code', "templates/smarty"); ?>" name="zip_code" class="form-control">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <textarea placeholder="<?php _lang('Message', "templates/smarty"); ?>" rows="9" class="form-control form_textarea" name="message" required></textarea>
            </div>
        </div>
    </fieldset>

    <div class="row">
        <?php if (get_option('disable_captcha', $params['id']) != 'y'): ?>
            <module type="captcha" />
        <?php endif; ?>
        <div class="col-xs-12 col-md-6">
            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> <?php _lang('SUBMIT', "templates/smarty"); ?></button>
        </div>
    </div>
</form>
