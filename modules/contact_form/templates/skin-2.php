<?php

/*

type: layout

name: Contact form in layout

description: Skin 2

*/

?>

<div class="alert alert-success margin-bottom-30" id="msg<?php print $form_id; ?>" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Thank You!</strong> Your message successfully sent!
</div>


<form class="nomargin sky-form boxed" data-form-id="<?php print $form_id ?>" name="<?php print $form_id ?>" method="post">
    <?php print csrf_form() ?>
    <input type="hidden" name="for" value="contact_form"/>
    <input type="hidden" name="for_id" value="<?php print $params['id']; ?>"/>

    <h3 style="padding: 10px 15px 15px 15px; margin-bottom:0;">
        <i class="fa fa-envelope"></i> <?php _lang('Contact', "templates/smarty"); ?>
    </h3>

    <fieldset class="nomargin">

        <label class="input">
            <i class="ico-prepend fa fa-user"></i>
            <input type="text" placeholder="<?php _lang('Name', "templates/smarty"); ?>" name="first_name" required>
        </label>

        <label class="input">
            <i class="ico-prepend fa fa-envelope"></i>
            <input type="email" placeholder="<?php _lang('Email', "templates/smarty"); ?>" name="email" required>
        </label>

        <label class="input">
            <i class="ico-prepend fa fa-phone"></i>
            <input type="tel" placeholder="<?php _lang('Phone', "templates/smarty"); ?>" name="phone">
        </label>

        <module type="custom_fields" data-id="<?php print $params['id'] ?>" data-for="module" template="skin-1" input_class="form-control"/>

        <label class="textarea">
            <i class="ico-prepend fa fa-comment"></i>
            <textarea rows="3" placeholder="<?php _lang('Type your question...', "templates/smarty"); ?>" name="message" required></textarea>
        </label>


        <?php if (get_option('disable_captcha', $params['id']) != 'y'): ?>
            <module type="captcha"/>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> <?php _lang('Send', "templates/smarty"); ?></button>
            </div>
        </div>
    </fieldset>

</form>
