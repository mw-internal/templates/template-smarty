<?php

/*

  type: layout

  name: Full Width Slider

  description: FullWitdth for bxSlider


*/

?>
<script>
    jQuery(document).ready(function () {
        var revapi;


        // Make Content Visible
        jQuery(".fullwidthbanner ul , .fullscreenbanner ul", '#<?php print $params['id'] ?>').removeClass('hide');


        /**
         @HALFSCREEN SLIDER
         **/
        if (jQuery(".fullwidthbanner", '#<?php print $params['id'] ?>').length > 0) {

            // Default Thumbs [small]
            var thumbWidth = 100,
                thumbHeight = 50,
                hideThumbs = 200,
                navigationType = "bullet",
                navigationArrows = "solo",
                navigationVOffset = 10;

            // Shadow
            _shadow = jQuery(".fullwidthbanner", '#<?php print $params['id'] ?>').attr('data-shadow') || 0;

            // Small Thumbnails
            if (jQuery(".fullwidthbanner", '#<?php print $params['id'] ?>').hasClass('thumb-small')) {
                var navigationType = "thumb";
            }

            // Large Thumbnails
            if (jQuery(".fullwidthbanner", '#<?php print $params['id'] ?>').hasClass('thumb-large')) {
                var navigationType = "thumb";
                thumbWidth = 195,
                    thumbHeight = 95,
                    hideThumbs = 0,
                    navigationArrows = "solo",
                    navigationVOffset = -94;

                // Hide thumbs on mobile - Avoid gaps
                /**
                 if(jQuery(window).width() < 800) {
					setTimeout(function() {
						var navigationVOffset = 10;
						jQuery("div.tp-thumbs").addClass('hidden');
					}, 100);
				}
                 **/
            }

            // Init Revolution Slider
            revapi = jQuery('.fullwidthbanner', '#<?php print $params['id'] ?>').revolution({
                dottedOverlay: "none",
                delay: 9000,
                startwidth: 1170,
                startheight: jQuery(".fullwidthbanner", '#<?php print $params['id'] ?>').attr('data-height') || 500,
                hideThumbs: hideThumbs,

                thumbWidth: thumbWidth,
                thumbHeight: thumbHeight,
                thumbAmount: parseInt(jQuery(".fullwidthbanner ul li").length) || 2,

                navigationType: navigationType,
                navigationArrows: navigationArrows,
                navigationStyle: jQuery('.fullwidthbanner', '#<?php print $params['id'] ?>').attr('data-navigationStyle') || "round", // round,square,navbar,round-old,square-old,navbar-old (see docu - choose between 50+ different item)

                touchenabled: "on",
                onHoverStop: "on",

                navigationHAlign: "center",
                navigationVAlign: "bottom",
                navigationHOffset: 0,
                navigationVOffset: navigationVOffset,

                soloArrowLeftHalign: "left",
                soloArrowLeftValign: "center",
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: "right",
                soloArrowRightValign: "center",
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                parallax: "mouse",
                parallaxBgFreeze: "on",
                parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

                shadow: parseInt(_shadow),
                fullWidth: "on",
                fullScreen: "off",

                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,

                spinner: "spinner0",
                shuffle: "off",

                autoHeight: "off",
                forceFullWidth: "off",

                hideThumbsOnMobile: "off",
                hideBulletsOnMobile: "on",
                hideArrowsOnMobile: "on",
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 768,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ""
            });

            // Used by styleswitcher onle - delete this on production!
            jQuery("#is_wide, #is_boxed").bind("click", function () {
                revapi.revredraw();
            });
        }
    });
</script>


<script type="text/javascript" src="<?php print template_url(); ?>/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php print template_url(); ?>/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>


<section id="slider" class="slider fullwidthbanner-container roundedcorners">
    <div class="fullwidthbanner" data-height="550" data-navigationStyle="">
        <ul class="hide">
            <?php foreach ($data as $slide) { ?>
                <?php if (isset($slide['skin_file'])) { ?>
                    <?php include $slide['skin_file'] ?>
                <?php } ?>
            <?php } ?>
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</section>
<!-- /SLIDER -->

