<?php

/*

  type: layout

  name: Swiper

  description: Swiper template for bxSlider


*/

?>
<script>
    jQuery(document).ready(function () {
        if (jQuery(".swiper-container", '#<?php print $params['id'] ?>').length > 0) {
            _swipperInit<?php print md5($params['id']) ?>();
        }
    });


    /**    _swipperInit()
     ******************************* **/
    function _swipperInit<?php print md5($params['id']) ?>() {

        var _swiper_container = jQuery(".swiper-container", '#<?php print $params['id'] ?>'),
            _effect = _swiper_container.attr('data-effect') || 'slide',
            _autoplay = _swiper_container.attr('data-autoplay') || false,
            _speed = _swiper_container.attr('data-speed') || 1000,
            _columns = _swiper_container.attr('data-columns') || 1,
            _loop = _swiper_container.attr('data-loop') || false;

        // Force Int
        if (_autoplay != false || _autoplay != 'false') {
            _autoplay = parseInt(_autoplay);
        }

        if (_loop == "true") {
            _loop = true;
        }

        if (_effect == 'cube') {
            var swiperSlider = new Swiper(_swiper_container, {
                effect: _effect,
                grabCursor: true,
                speed: parseInt(_speed),
                cube: {
                    shadow: true,
                    slideShadows: true,
                    shadowOffset: 20,
                    shadowScale: 0.94
                }
            });
        } else {
            var swiperSlider = new Swiper(_swiper_container, {
                pagination: '#<?php print $params['id'] ?> .swiper-pagination',
                paginationClickable: '#<?php print $params['id'] ?> .swiper-pagination',
                nextButton: '#<?php print $params['id'] ?> .swiper-button-next',
                prevButton: '#<?php print $params['id'] ?> .swiper-button-prev',
                spaceBetween: 1,
                speed: parseInt(_speed),
                loop: _loop,
                autoplay: _autoplay || false,
                effect: _effect,
                grabCursor: false,
                slidesPerView: parseInt(_columns)
            });

        }

    }
</script>

<link rel="stylesheet" type="text/css" href="<?php print template_url(); ?>/assets/plugins/slider.swiper/dist/css/swiper.min.css"/>

<section id="slider" class="fullheight">

    <!--
        SWIPPER SLIDER PARAMS

        data-effect="slide|fade|coverflow"
        data-autoplay="2500|false"
    -->
    <div class="swiper-container" data-effect="slide" data-autoplay="false">
        <div class="swiper-wrapper">

            <?php foreach ($data as $slide) { ?>
                <div class="swiper-slide" style="background-image: url('<?php print $slide['images'][0]; ?>');">
                    <?php if (isset($slide['skin_file'])) { ?>
                        <?php include $slide['skin_file'] ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>

        <!-- Swiper Pagination -->
        <div class="swiper-pagination"></div>

        <!-- Swiper Arrows -->
        <div class="swiper-button-next"><i class="icon-angle-right"></i></div>
        <div class="swiper-button-prev"><i class="icon-angle-left"></i></div>
    </div>

</section>
<!-- /SLIDER -->

