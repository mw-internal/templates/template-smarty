<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>

<div class="display-table">
    <div class="display-table-cell vertical-align-middle">
        <div class="container">

            <div class="row">
                <div class="text-center col-md-8 col-xs-12 col-md-offset-2">

                    <h1 class="bold font-raleway wow fadeInUp" data-wow-delay="0.4s"><?php print $slide['primaryText']; ?></h1>
                    <p class="lead font-lato weight-300 hidden-xs wow fadeInUp" data-wow-delay="0.6s"><?php print $slide['secondaryText']; ?></p>
                    <?php if (isset($slide['url']) AND $slide['url'] != ''): ?>
                        <br/>
                        <br/>
                        <a href="<?php print $slide['url']; ?>" class="btn btn-default btn-lg"><?php print $slide['seemoreText']; ?></a>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>