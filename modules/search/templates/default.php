<?php

/*

type: layout

name: Default

description: Default Search template

*/

?>

<div class="inline-search clearfix margin-bottom-30">
    <form action="<?php print site_url(); ?>search.php" method="get" class="widget_search">
        <input type="search" placeholder="Start Searching..." id="keywords" name="keywords" class="serch-input">
        <button type="submit">
            <i class="fa fa-search"></i>
        </button>
    </form>
</div>