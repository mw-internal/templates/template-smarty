<?php

/*

type: layout

name: Footer - bottom

description: Footer - bottom menu

*/

?>

<?php
$menu_filter['ul_class'] = 'pull-right nomargin list-inline mobile-block';
$menu_filter['ul_class_deep'] = '';
$menu_filter['li_class'] = '';
$menu_filter['a_class'] = '';

$mt = menu_tree($menu_filter);

if ($mt != false) {
    print ($mt);
} else {
    print lnotif("There are no items in the menu <b>" . $params['menu-name'] . '</b>');
}
?>
