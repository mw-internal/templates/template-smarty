<?php

/*

type: layout

name: Header

description: Header navigation

*/

?>

<div class="side-nav margin-bottom-60">

    <div class="side-nav-head">
        <button class="fa fa-bars"></button>
        <h4 class="edit" rel="module" field="title-<?php print $params['id']; ?>">NAVIGATION</h4>
    </div>

    <?php
    $menu_filter['ul_class'] = 'list-group list-group-bordered list-group-noicon uppercase';
    $menu_filter['ul_id'] = '';
    $menu_filter['ul_class_deep'] = '';

    $menu_filter['li_class'] = 'list-group-item';
    $menu_filter['li_submenu_class'] = '';
    $menu_filter['a_class'] = '';
    $menu_filter['li_submenu_a_class'] = 'dropdown-toggle';


    $mt = menu_tree($menu_filter);

    if ($mt != false) {
        print ($mt);
    } else {
        print lnotif("There are no items in the menu <b>" . $params['menu-name'] . '</b>');
    }
    ?>
</div>