<div class="row">
    <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
        <form class="nomargin sky-form boxed" action="https://www.astechprocessing.net/ASTECH/Payments/ASTECHPayments.aspx" method="post" id="credit-card-3ds-form" accept-charset="UTF-8">
            <fieldset class="nomargin">
                <label class="input">
                    <i class="ico-prepend fa fa-envelope"></i>
                    <input required="required" placeholder="Email" type="text" id="edit-clientemail" name="ClientEmail" value="" size="40" maxlength="128" class="form-text required">
                </label>

                <label class="input">
                    <i class="ico-prepend fa fa-user"></i>
                    <input required="required" type="text" placeholder="Name on card" id="edit-clientnameoncard" name="ClientNameOnCard" value="" size="40" maxlength="16" class="form-text required">
                </label>

                <label class="input">
                    <i class="ico-prepend fa fa-usd"></i>
                    <input required="required" placeholder="Amount(USD)" type="text" id="edit-amount" name="Amount" value="" size="10" maxlength="250" class="form-text required">
                </label>

                <input type="hidden" name="Currency" value="USD">
                <input type="hidden" name="MerchantID" value="WEBT000702">
                <input type="hidden" name="BrandID" value="T3552T_Live">
                <input type="hidden" name="StoreID" value="ifa-fxStore">
                <input type="hidden" name="ServiceName" value="IFA-FX pay">
                <input type="hidden" name="OrderNumber" value="1513935399">
                <input type="hidden" name="ProductType" value="CREDIT">
                <input type="hidden" name="RequestType" value="0">
                <input type="hidden" name="GUIType" value="0">
                <input type="hidden" name="ClientCountry" value="BG">
                <input type="hidden" name="form_build_id" value="form-tuQuUFlM5aoxNMNFFt9vf3ylwD5H4O2mfTH9o4JDtdQ">
                <input type="hidden" name="form_id" value="credit_card_3ds_form">

                <div class="row">
                    <div class="">
                        <button type="submit" id="edit-submit" name="op" class="btn btn-primary"><i class="fa fa-check"></i> <?php _lang('Payment', "templates/smarty") ?></button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>