<section class="edit nodrop safe-mode" field="ifa-layout-1-<?php print $params['id'] ?>" rel="module" style="background:url('<?php print template_url('assets/images/'); ?>demo/wall2.jpg')">
    <div class="display-table">
        <div class="display-table-cell vertical-align-middle">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-md-7 col-sm-7 col-lg-8 allow-drop">
                        <h2 class="size-20 text-center-xs">Why Smarty?</h2>

                        <p>Lorem ipsum dolor sit amet. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.Lorem ipsum dolor
                            sit amet. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>

                        <ul class="list-unstyled login-features">
                            <li class="cloneable">
                                <i class="fa fa-globe"></i> <strong>Lorem ipsum</strong> dolor sit amet.
                            </li>
                            <li class="cloneable">
                                <i class="fa  fa-globe"></i> <strong>Sed ut perspiciatis</strong> unde omnis iste.
                            </li>
                            <li class="cloneable">
                                <i class="fa  fa-globe"></i> <strong>Et harum quidem</strong> rerum facilis est et expedita distinctio.
                            </li>
                            <li class="cloneable">
                                <i class="fa fa-globe"></i> <strong>Nam libero</strong> tempore, cum soluta nobis.
                            </li>
                            <li class="cloneable">
                                <i class="fa  fa-globe"></i> <strong>Est eligendi</strong> voluptatem accusantium.
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">

                        <form class="nomargin sky-form boxed" action="https://www.astechprocessing.net/ASTECH/Payments/ASTECHPayments.aspx" method="post" id="credit-card-3ds-form"
                              accept-charset="UTF-8">
                            <h3 style="padding: 10px 15px 15px 15px; margin-bottom:0;">
                                <i class="fa fa-envelope"></i> <?php _lang('3D Secure Payment', "templates/smarty"); ?>
                            </h3>

                            <fieldset class="nomargin">
                                <label class="input">
                                    <i class="ico-prepend fa fa-envelope"></i>
                                    <input required="required" placeholder="Email" type="text" id="edit-clientemail" name="ClientEmail" value="" size="40" maxlength="128" class="form-text required">
                                </label><br/>

                                <label class="input">
                                    <i class="ico-prepend fa fa-user"></i>
                                    <input required="required" type="text" placeholder="Name on card" id="edit-clientnameoncard" name="ClientNameOnCard" value="" size="40" maxlength="16"
                                           class="form-text required">
                                </label><br/>

                                <label class="input">
                                    <i class="ico-prepend fa fa-usd"></i>
                                    <input required="required" placeholder="Amount(USD)" type="text" id="edit-amount" name="Amount" value="" size="10" maxlength="250" class="form-text required">
                                </label><br/>

                                <input type="hidden" name="Currency" value="USD">
                                <input type="hidden" name="MerchantID" value="WEBT000702">
                                <input type="hidden" name="BrandID" value="T3552T_Live">
                                <input type="hidden" name="StoreID" value="ifa-fxStore">
                                <input type="hidden" name="ServiceName" value="IFA-FX pay">
                                <input type="hidden" name="OrderNumber" value="1513935399">
                                <input type="hidden" name="ProductType" value="CREDIT">
                                <input type="hidden" name="RequestType" value="0">
                                <input type="hidden" name="GUIType" value="0">
                                <input type="hidden" name="ClientCountry" value="BG">
                                <input type="hidden" name="form_build_id" value="form-tuQuUFlM5aoxNMNFFt9vf3ylwD5H4O2mfTH9o4JDtdQ">
                                <input type="hidden" name="form_id" value="credit_card_3ds_form">

                                <div class="row">
                                    <div class="">
                                        <button type="submit" id="edit-submit" name="op" class="btn btn-primary"><i class="fa fa-check"></i> <?php _lang('Payment', "templates/smarty") ?></button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

