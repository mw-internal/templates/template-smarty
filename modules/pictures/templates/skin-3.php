<?php

/*

type: layout

name: Skin 3

description: Skin 3

*/

?>

<?php if (is_array($data)): ?>
    <?php $rand = uniqid(); ?>

    <ul class="row clients-dotted list-inline">
        <?php $count = -1;
        foreach ($data as $item): ?>
            <?php $count++; ?>
            <li class="col-md-5th col-sm-5th col-xs-6 pictures picture-<?php print $item['id']; ?>">
                <img class="img-responsive" src="<?php print thumbnail($item['filename'], 137, 77, true); ?>" alt=""/>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>