<?php

/*

type: layout

name: Product Inner - Gallery

description: Skin 2

*/

?>

<?php if (is_array($data)): ?>

    <?php $rand = uniqid(); ?>

    <?php foreach ($data as $key => $item): ?>
        <?php if ($key == 0): ?>
            <div class="thumbnail relative margin-bottom-3">
                <!-- IMAGE ZOOM
                   data-mode="mouseover|grab|click|toggle"-->
                <figure id="zoom-primary" class="zoom" data-mode="mouseover">
                    <!--
                        zoom buttton

                        positions available:
                            .bottom-right
                            .bottom-left
                            .top-right
                            .top-left
                    -->
                    <a class="lightbox bottom-right" href="<?php print thumbnail($item['filename'], 900); ?>" data-plugin-options='{"type":"image"}'><i
                                class="glyphicon glyphicon-search"></i></a>

                    <!--
                        image

                        Extra: add .image-bw class to force black and white!
                    -->
                    <img class="img-responsive" src="<?php print thumbnail($item['filename'], 600); ?>" width="1200" height="1500" alt="This is the product title"/>
                </figure>

            </div>
        <?php endif; ?>
    <?php endforeach; ?>


    <!-- Thumbnails (required height:100px) -->
    <div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured"
         data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}' id="product-gallery-<?php print $rand; ?>">
        <?php foreach ($data as $key => $item): ?>
                <a class="thumbnail <?php if ($key == 0): ?>active<?php endif; ?>" href="<?php print thumbnail($item['filename'], 600); ?>">
                    <img src="<?php print thumbnail($item['filename'], 150); ?>" height="100" alt=""/>
                </a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>