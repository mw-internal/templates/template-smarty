<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="mw-user-logged-holder">
	<div class="mw-user-welcome">
		<?php _lang("Welcome", "templates/smarty"); ?>
		<?php print user_name(); ?> </div>
	<a href="<?php print site_url() ?>">
	<?php _lang("Go to", "templates/smarty"); ?>
	<?php print site_url() ?></a><br />
 <a href="<?php print api_link('logout') ?>" >
	<?php _lang("Log Out", "templates/smarty"); ?>
	</a>
	<?php if(is_admin()): ?>
	<div class="mw-user-logged-holder"> <a href="<?php print admin_url() ?>">
		<?php _lang("Admin panel", "templates/smarty"); ?>
		</a>  </div>
	<?php endif; ?>
</div>
