<?php

/*

type: layout

name: Register form for Layout 32

description: Skin 1

*/

?>
<?php if (is_logged() == false): ?>
    <script type="text/javascript">
        mw.moduleCSS("<?php print modules_url(); ?>users/users_modules.css");
        mw.require('forms.js', true);
        mw.require('url.js', true);
        $(document).ready(function () {
            mw.$('#user_registration_form_holder').submit(function () {
                mw.form.post(mw.$('#user_registration_form_holder'), '<?php print site_url('api') ?>/user_register', function () {
                    mw.response('#register_form_holder', this);
                    if (typeof this.success !== 'undefined') {
                        mw.form.post(mw.$('#user_registration_form_holder'), '<?php print site_url('api') ?>/user_login', function () {
                            mw.load_module('users/login', '#<?php print $params['id'] ?>');
                            window.location.href = window.location.href;
                        });
                    }
                });
                return false;
            });
        });
    </script>
    <div id="register_form_holder">
        <form class="nomargin sky-form boxed" action="#" id="user_registration_form_holder" method="post">
            <?php print csrf_form(); ?>
            <h3 style="padding: 10px 15px 15px 15px; margin-bottom:0;">
                <i class="fa fa-users"></i> Register
            </h3>

            <fieldset class="nomargin">
                <label class="input">
                    <i class="ico-append fa fa-envelope"></i>
                    <input type="text" name="email" required placeholder="<?php _lang("Email address", "templates/smarty"); ?>">
                    <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                </label>

                <label class="input">
                    <i class="ico-append fa fa-lock"></i>
                    <input required="" type="password" name="password" placeholder="<?php _lang("Password", "templates/smarty"); ?>">
                    <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                </label>

                <?php if ($form_show_password_confirmation): ?>
                    <label class="input">
                        <i class="ico-append fa fa-lock"></i>
                        <input required="" type="password" name="password2" placeholder="<?php _lang("Confirm password", "templates/smarty"); ?>">
                        <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                    </label>
                <?php endif; ?>

                <div class="row">
                    <?php if ($form_show_first_name): ?>
                        <div class="col-md-6">
                            <label class="input">
                                <input type="text" name="first_name" required placeholder="<?php _lang("First name", "templates/smarty"); ?>">
                            </label>
                        </div>
                    <?php endif; ?>
                    <?php if ($form_show_last_name): ?>
                        <div class="col col-md-6">
                            <label class="input">
                                <input type="text" name="last_name" required placeholder="<?php _lang("Last name", "templates/smarty"); ?>">
                            </label>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="margin-top-20">
                    <label class="checkbox nomargin"><input class="checked-agree" type="checkbox" name="tos_agree">
                        <i></i>I agree to the
                        <a href="#">Terms of Service</a>
                    </label>
                </div>

                <?php if (!$captcha_disabled): ?>
                    <module type="captcha"/>
                <?php endif; ?>
            </fieldset>

            <div class="row margin-bottom-20">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> <?php print $form_btn_title ?></button>
                </div>
            </div>

        </form>
    </div>
<?php endif; ?>