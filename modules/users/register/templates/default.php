<?php

/*

type: layout

name: Default

description: Default

*/

?>
<?php if (is_logged() == false): ?>
    <script type="text/javascript">
        mw.moduleCSS("<?php print modules_url(); ?>users/users_modules.css");
        mw.require('forms.js', true);
        mw.require('url.js', true);
        $(document).ready(function () {
            mw.$('#user_registration_form_holder').submit(function () {
                mw.form.post(mw.$('#user_registration_form_holder'), '<?php print site_url('api') ?>/user_register', function () {
                    mw.response('#register_form_holder', this);
                    if (typeof this.success !== 'undefined') {
                        mw.form.post(mw.$('#user_registration_form_holder'), '<?php print site_url('api') ?>/user_login', function () {
                            mw.load_module('users/login', '#<?php print $params['id'] ?>');
                            window.location.href = window.location.href;
                        });
                    }
                });
                return false;
            });
        });
    </script>

    <div class="box-static box-transparent box-bordered padding-30" id="register_form_holder">
        <div class="box-title margin-bottom-30">
            <h2 class="size-20"><?php _lang("Don't have an account yet?", "templates/smarty"); ?></h2>
        </div>

        <form class="nomargin sky-form" action="#" id="user_registration_form_holder" method="post">
            <?php print csrf_form(); ?>

            <fieldset>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6">
                            <label for="register:email">Email *</label>
                            <label class="input margin-bottom-10">
                                <i class="ico-append fa fa-envelope"></i>
                                <input required="" type="text" name="email" placeholder="">
                                <b class="tooltip tooltip-bottom-right"><?php _lang("Your Email", "templates/smarty"); ?></b>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">

                        <?php if ($form_show_first_name): ?>
                            <div class="col-md-6 col-sm-6">
                                <label>First Name *</label>
                                <label class="input margin-bottom-10">
                                    <i class="ico-append fa fa-user"></i>
                                    <input required="" type="text" name="first_name">
                                    <b class="tooltip tooltip-bottom-right"><?php _lang("Your First Name", "templates/smarty"); ?></b>
                                </label>
                            </div>
                        <?php endif; ?>

                        <?php if ($form_show_last_name): ?>
                            <div class="col-md-6 col-sm-6">
                                <label for="register:last_name"><?php _lang("Last Name", "templates/smarty"); ?> *</label>
                                <label class="input margin-bottom-10">
                                    <i class="ico-append fa fa-user"></i>
                                    <input required="" type="text" name="last_name">
                                    <b class="tooltip tooltip-bottom-right">Your Last Name</b>
                                </label>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6">
                            <label for="register:pass1"><?php _lang("Password", "templates/smarty"); ?> *</label>
                            <label class="input margin-bottom-10">
                                <i class="ico-append fa fa-lock"></i>
                                <input required="" type="password" name="password">
                                <b class="tooltip tooltip-bottom-right">Min. 6 characters</b>
                            </label>
                        </div>

                        <?php if ($form_show_password_confirmation): ?>
                            <div class="col-md-6 col-sm-6">
                                <label for="register:pass2"><?php _lang("Password Again", "templates/smarty"); ?> *</label>
                                <label class="input margin-bottom-10">
                                    <i class="ico-append fa fa-lock"></i>
                                    <input required="" type="password" name="password2">
                                    <b class="tooltip tooltip-bottom-right">Type the password again</b>
                                </label>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if (!$captcha_disabled): ?>
                    <module type="captcha" />
                <?php endif; ?>

                <hr/>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> <?php print $form_btn_title ?></button>
                    </div>
                </div>
            </fieldset>


        </form>
    </div>
<?php endif; ?>