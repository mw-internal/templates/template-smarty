<?php

/*

type: layout

name: Default

description: Default

*/

?>
<?php $user = user_id(); ?>
<?php $have_social_login = false; ?>

<?php if ($user != false): ?>
    <div class="box-static box-border-top padding-30">
        <module type="users/profile"/>
    </div>
<?php else: ?>
    <div class="box-static box-border-top padding-30" id="user_login_holder_<?php print $params['id'] ?>">
        <div class="box-title margin-bottom-30">
            <h2 class="size-20">I'm a returning customer</h2>
        </div>

        <div class="alert alert-mini alert-danger margin-bottom-30" style="margin: 0;display: none;"></div>

        <form class="sky-form" method="post" id="user_login_<?php print $params['id'] ?>" action="#" autocomplete="off">
            <div class="clearfix">


                <!-- Email -->
                <div class="form-group">
                    <label><?php _lang("Email or username", "templates/smarty"); ?></label>
                    <label class="input margin-bottom-10">
                        <i class="ico-append fa fa-envelope"></i>
                        <input required="" type="text" name="username">
                        <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                    </label>
                </div>

                <!-- Password -->
                <div class="form-group">
                    <label><?php _lang("Password", "templates/smarty"); ?></label>
                    <label class="input margin-bottom-10">
                        <i class="ico-append fa fa-lock"></i>
                        <input required="" type="password" name="password">
                        <b class="tooltip tooltip-bottom-right">Type your account password</b>
                    </label>
                </div>

                <?php if (isset($login_captcha_enabled) and $login_captcha_enabled): ?>
                    <module type="captcha" />
                <?php endif; ?>

            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="form-tip pt-20">
                        <a class="no-text-decoration size-13 margin-top-10 block" href="<?php print forgot_password_url(); ?>"><?php _lang("Forgot Password?", "templates/smarty"); ?></a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php _lang("LOGIN", "templates/smarty"); ?></button>
                </div>
            </div>
        </form>

        <hr/>

        <?php
        # Login Providers
        $facebook = get_option('enable_user_fb_registration', 'users') == 'y';
        $twitter = get_option('enable_user_twitter_registration', 'users') == 'y';
        $google = get_option('enable_user_google_registration', 'users') == 'y';
        $windows = get_option('enable_user_windows_live_registration', 'users') == 'y';
        $github = get_option('enable_user_github_registration', 'users') == 'y';
        $mw_login = get_option('enable_user_microweber_registration', 'users') == 'y';

        if ($facebook or $twitter or $google or $windows or $github) {
            $have_social_login = true;
        } else {
            $have_social_login = false;
        }
        ?>

        <?php if ($have_social_login): ?>


            <div class="text-center">
                <div class="margin-bottom-20">&ndash; <?php _lang("OR", "templates/smarty"); ?> &ndash;</div>

                <?php if ($facebook): ?>
                    <a href="<?php print api_link('user_social_login?provider=facebook') ?>" class="btn btn-block btn-social btn-facebook margin-top-10">
                        <i class="fa fa-facebook"></i> Sign in with Facebook
                    </a>
                <?php endif; ?>

                <?php if ($twitter): ?>
                    <a href="<?php print api_link('user_social_login?provider=twitter') ?>" class="btn btn-block btn-social btn-twitter margin-top-10">
                        <i class="fa fa-twitter"></i> Sign in with Twitter
                    </a>
                <?php endif; ?>

                <?php if ($google): ?>
                    <a href="<?php print api_link('user_social_login?provider=google') ?>" class="btn btn-block btn-social btn-google margin-top-10">
                        <i class="fa fa-google"></i> Sign in with Google+
                    </a>
                <?php endif; ?>

                <?php if ($github): ?>
                    <a href="<?php print api_link('user_social_login?provider=github') ?>" class="btn btn-block btn-social btn-github margin-top-10">
                        <i class="fa fa-github"></i> Sign in with Github
                    </a>
                <?php endif; ?>

                <?php if ($mw_login): ?>
                    <a href="<?php print api_link('user_social_login?provider=github') ?>" class="btn btn-block btn-social btn-twitter margin-top-10">
                        <i class="mw-icon-mw"></i> Sign in with Microweber
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>

    </div>
<?php endif; ?>

