<?php

/*

type: layout
content_type: dynamic
name: Search
description: search layout
position: 4

*/


?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="smarty_content">
    <div class="nodrop safe-mode">
        <?php $header_style = get_option('header-style', 'mw-template-smarty'); ?>
        <?php if ($header_style == '' OR $header_style == 'clean'): ?>
            <section class="page-header <?php $header_breadcrumb_small = get_option('header-breadcrumb-small', 'mw-template-smarty');
            if ($header_breadcrumb_small == 'true'): ?>page-header-xs<?php endif; ?>">
                <div class="container">
                    <h1 class="edit" field="title" rel="content">Search results</h1>

                    <module type="breadcrumb" id="search-results-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php elseif ($header_style == 'background'): ?>
            <section class="page-header page-header-lg parallax parallax-4" style="background-image:url('<?php print template_url('assets/images/'); ?>hero.jpg')">
                <div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

                <div class="container">
                    <h1 class="edit" field="title" rel="content">Search results</h1>
                    <span class="font-lato size-18 weight-300 edit" field="title-info" rel="content">We believe in Simple &amp; Creative</span>

                    <module type="breadcrumb" id="search-results-breadcrumbs-<?php print PAGE_ID; ?>"/>
                </div>
            </section>
        <?php endif; ?>
    </div>

    <section class="nodrop">
        <div class="container">
            <?php
            $keywords = '';
            if (isset($_GET['keywords'])) {
                $keywords = htmlspecialchars($_GET['keywords']);
            }
            ?>
            <h3><?php _lang('Results for', "templates/smarty"); ?> <?php print $keywords; ?> in Shop</h3>
            <module type="shop/products" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
            <br/>
            <br/>
            <h3><?php _lang('Results for', "templates/smarty"); ?> <?php print $keywords; ?> in Blog</h3>
            <module type="posts" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
        </div>
    </section>
</div>

<?php include template_dir() . "footer.php"; ?>
