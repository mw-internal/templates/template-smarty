<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
    <title>{content_meta_title}</title>
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/><![endif]-->
    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <script>mw.lib.require('bootstrap3');</script>

    <script>
        mw.require('icon_selector.js');

        mw.iconLoader()
            .addIconSet('fontAwesome')
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        AddToCartModalContent = window.AddToCartModalContent || function (title) {
                var html = ''

                    + '<section style="text-align: center;">'
                    + '<h5>' + title + '</h5>'
                    + '<p><?php _e("has been added to your cart"); ?></p>'
                    + '<a href="javascript:;" onclick="mw.tools.modal.remove(\'#AddToCartModal\')" class="btn btn-default"><?php _e("Continue shopping"); ?></a> &nbsp;'
                    + '<a href="<?php print checkout_url(); ?>" class="btn btn-warning"><?php _e("Checkout"); ?></a></section>';

                return html;
            }
    </script>


    <!-- WEB FONTS : use %7C instead of | (pipe) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css"/>


    <!-- SWIPER SLIDER -->
    <link href="{TEMPLATE_URL}assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css"/>

    <!-- REVOLUTION SLIDER -->
    <link href="{TEMPLATE_URL}assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css"/>
    <link href="{TEMPLATE_URL}assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css"/>

    <!--
        AVAILABLE BODY CLASSES:

        smoothscroll 			= create a browser smooth scroll
        enable-animation		= enable WOW animations

        bg-grey					= grey background
        grain-grey				= grey grain background
        grain-blue				= blue grain background
        grain-green				= green grain background
        grain-blue				= blue grain background
        grain-orange			= orange grain background
        grain-yellow			= yellow grain background

        boxed 					= boxed layout
        pattern1 ... patern11	= pattern background
        menu-vertical-hide		= hidden, open on click

        BACKGROUND IMAGE [together with .boxed class]
        data-background="assets/images/boxed_background/1.jpg"
    -->

    <!-- PAGE LEVEL SCRIPTS -->
    <link href="{TEMPLATE_URL}assets/css/header-1.css" rel="stylesheet" type="text/css"/>
    <link href="{TEMPLATE_URL}assets/css/layout-shop.css" rel="stylesheet" type="text/css"/>

    <!-- THEME CSS -->
    <link href="{TEMPLATE_URL}assets/css/essentials.css" rel="stylesheet" type="text/css"/>
    <link href="{TEMPLATE_URL}assets/css/layout.css" rel="stylesheet" type="text/css"/>

    <?php $color_scheme = get_option('color-scheme', 'mw-template-smarty'); ?>
    <?php if ($color_scheme == '' OR $color_scheme == 'green'): ?>
        <link href="{TEMPLATE_URL}assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme"/>
    <?php else: ?>
        <link href="{TEMPLATE_URL}assets/css/color_scheme/<?php print $color_scheme; ?>.css" rel="stylesheet" type="text/css" id="color_scheme"/>
    <?php endif; ?>

    <?php $layout_style = get_option('layout-style', 'mw-template-smarty'); ?>
    <?php if ($layout_style != '' AND $layout_style != 'light'): ?>
        <link id="css_dark_skin" href="{TEMPLATE_URL}assets/css/layout-dark.css" rel="stylesheet" type="text/css" title="dark">
    <?php endif; ?>

    <!--    <script type="text/javascript" src="{TEMPLATE_URL}js/main.js"></script>-->

    <?php $rtl_is_on = get_option('rtl_is_on', 'mw-template-smarty'); ?>
    <?php if ($rtl_is_on == 'true'): ?>
        <link href="{TEMPLATE_URL}assets/plugins/bootstrap/RTL/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" id="rtl_ltr_b1"/>
        <link href="{TEMPLATE_URL}assets/plugins/bootstrap/RTL/bootstrap-flipped.min.css" rel="stylesheet" type="text/css" id="rtl_ltr_b2"/>
        <link href="{TEMPLATE_URL}assets/css/layout-RTL.css" rel="stylesheet" type="text/css" id="rtl_ltr"/>
    <?php endif; ?>

    <?php $layout_type = get_option('layout-type', 'mw-template-smarty'); ?>
    <script>
        $(window).on('load', function () {
            if (mw.iconSelector) {
                mw.iconSelector.addCSS('link[href*="/iconsmind.css"]', '.icon-')
            }
        });
    </script>
</head>
<body class="<?php print helper_body_classes(); ?> smoothscroll enable-animation <?php if ($layout_type != '' AND $layout_type != 'wide') {
    print $layout_type;
} ?>">


<!-- SLIDE TOP -->
<?php /*<div id="slidetop">

    <div class="container">

        <div class="row">

            <div class="col-md-4">
                <h6><i class="icon-heart"></i> WHY SMARTY?</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor
                    convallis et, iaculis ac massa. </p>
            </div>

            <div class="col-md-4">
                <h6><i class="icon-attachment"></i> RECENTLY VISITED</h6>
                <ul class="list-unstyled">
                    <li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> This is a very long text, very very very very very very very very very very very very </a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Lorem ipsum dolor sit amet</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Dolor sit amet,consectetur adipiscing elit amet</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet,consectetur adipiscing elit</a></li>
                </ul>
            </div>

            <div class="col-md-4">
                <h6><i class="icon-envelope"></i> CONTACT INFO</h6>
                <ul class="list-unstyled">
                    <li><b>Address:</b> PO Box 21132, Here Weare St, <br/> Melbourne, Vivas 2355 Australia</li>
                    <li><b>Phone:</b> 1-800-565-2390</li>
                    <li><b>Email:</b> <a href="mailto:support@yourname.com">support@yourname.com</a></li>
                </ul>
            </div>

        </div>

    </div>

    <a class="slidetop-toggle" href="#"><!-- toggle button --></a>

</div>*/
?>
<!-- /SLIDE TOP -->


<!-- wrapper -->
<div id="wrapper">

    <!--
        AVAILABLE HEADER CLASSES

        Default nav height: 96px
        .header-md 		= 70px nav height
        .header-sm 		= 60px nav height

        .noborder 		= remove bottom border (only with transparent use)
        .transparent	= transparent header
        .translucent	= translucent header
        .sticky			= sticky header
        .static			= static header
        .dark			= dark header
        .bottom			= header on bottom

        shadow-before-1 = shadow 1 header top
        shadow-after-1 	= shadow 1 header bottom
        shadow-before-2 = shadow 2 header top
        shadow-after-2 	= shadow 2 header bottom
        shadow-before-3 = shadow 3 header top
        shadow-after-3 	= shadow 3 header bottom

        .clearfix		= required for mobile menu, do not remove!

        Example Usage:  class="clearfix sticky header-sm transparent noborder"
    -->
    <div id="header" class="sticky clearfix">

        <!-- TOP NAV -->
        <header id="topNav">
            <div class="container">
                <?php $top_bar = get_option('top-bar', 'mw-template-smarty'); ?>
                <?php if ($top_bar == 'true'): ?>
                    <div class="top-header">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6 text-right edit safe-mode" field="template-header-phone" rel="global">
                            <p><i class="fa fa-phone"></i> 0123 456 789</p>
                        </div>
                    </div>
                <?php endif; ?>

                <!-- Mobile Menu Button -->
                <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>

                <ul class="pull-right nav nav-pills nav-second-main">
                    <?php $search_field = get_option('search-field', 'mw-template-smarty'); ?>
                    <?php if ($search_field == 'true'): ?>
                        <li class="search">
                            <a href="javascript:;">
                                <i class="fa fa-search"></i>
                            </a>
                            <div class="search-box">
                                <form action="<?php print site_url(); ?>search.php" method="get">
                                    <div class="input-group">
                                        <input type="text" name="keywords" placeholder="Search" class="form-control"/>
                                        <span class="input-group-btn"><button class="btn btn-primary" type="submit"><?php _lang('Search', "templates/smarty"); ?></button></span>
                                    </div>
                                </form>
                            </div>
                        </li>
                    <?php endif; ?>

                    <?php $shopping_cart = get_option('shopping-cart', 'mw-template-smarty'); ?>
                    <?php if ($shopping_cart == 'true'): ?>
                        <li class="quick-cart">
                            <a href="#">
                                <span class="badge badge-aqua btn-xs badge-corner"><?php print cart_sum(false); ?></span>
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                            <div class="quick-cart-box">
                                <module type="shop/cart" template="small" id="cart-bag" class=""/>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>

                <module type="logo" id="logo_header" default-text="Smarty" class="logo pull-left"/>
                <module type="menu" name="header_menu" id="header_menu" class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark" template="header"/>
            </div>
        </header>
        <!-- /Top Nav -->

    </div>



